import axios from 'axios'
import { host } from '../../config';


export const getConnectedPagesProject = () => {
    return (dispatch) => {
    
        return axios.get(host + `/api/v1/secure/project/get/user`, {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('all projects for this user', res.data.data)
            
            dispatch({
                type: 'GET_WIZARD_ACTIVE_PAGE_POSTS',
                payload: res.data.data
            })
        })
        .catch((err) => console.log("err getConnectedPagesProject", err))
    }
};

export const addPageEntity = (page) => {
    return (dispatch) => {
        
        return axios.get(host + `/api/v1/secure/pages/entities/add/${page.id}`, {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('add entity', res.data.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err addPageEntity", err))
    }
};

export const getPageEntities = (page) => {
    return (dispatch) => {
        
        return axios.get(host + `/api/v1/secure/project/get/entitiesproject/${page.platform}/${page.id}`, {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            // console.log('entities mtaaa page', res.data.data);
            
            dispatch({
                type: 'GET_ENTITIES_OF_A_PAGE',
                payload: res.data.data
            })
        })
        .catch((err) => console.log("err getPageEntities", err))
    }
};


// TEAM:
export const sendMailTeam = (wizardSelectedPage,emailTeamMember) => {
    return (dispatch) => {
    
        return axios.post(host + `/api/v1/secure/membership/sendMail/${wizardSelectedPage.id}/${wizardSelectedPage.platform}/`, {
            email: emailTeamMember,
        },
        {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('Sending the maiiiiiil  => ', res.data.data)
            
            // dispatch({
            //     type: 'GET_ALL_PRODUCT_CATEGORIES',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err getConnectedPagesProject", err))
    }
};

export const inviteMember = (page) => {
    return (dispatch) => {
        
        return axios.get(host + `/api/v1/secure/membership/get/${page.id}/${page.platform}`, {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        // .then(res => {
        //     console.log('Token of invitation', res.data.data);
        
        //     // dispatch({
        //     //     type: 'GET_ENTITIES_OF_A_PAGE',
        //     //     payload: res.data.data
        //     // })
        // })
        // .catch((err) => console.log("err inviteMember", err))
    }
};

export const deleteTeamMember = (page,idFacebook) => {
    return (dispatch) => {
       let type = page.platform;
       let idPage = page.id;

        return axios.post(host + `/api/v1/secure/membership/deleteMember/`,{
            idFacebook,
            idPage,
            type,
        },{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            // console.log('deleteTeamMember', res.data.data);
        
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("deleteTeamMember error", err))
    }
};


/* INSTAGRAM */
export const connectInstagramPage = (idPage) => {
    return (dispatch) => {

        let page = {
            id: idPage,
        };
        
        return axios.post(host + `/api/v1/secure/pagesInsta/connect/`,page,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('retour de connect page insta', res.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err connect page insta", err))
    }
};

export const disconnectInstagramPage = (idPage) => {
    return (dispatch) => {
     
        let page = {
            id: idPage,
        };
        
        return axios.post(host + `/api/v1/secure/pagesInsta/disconnect/disconnect`,page,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('retour de disconnect page insta', res.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err disconnect page instagram", err))
    };
};

export const deleteInstagramPage = (idPage) => {
    return (dispatch) => {
     
        let page = {
            id: idPage,
        };
        
        return axios.post(host + `/api/v1/secure/pagesInsta/disconnect/delete`,page,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('retour de disconnect page insta', res.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err disconnect page instagram", err))
    };
};

export const makeOwnerInstagramPage = (idPage,idFacebook) => {
    return (dispatch) => {
     
        let page = {
            id: idPage,
            idFacebook: idFacebook // idFacebook of the new owner
        };
        
        return axios.post(host + `/api/v1/secure/pagesInsta/disconnect/owner`,page,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('retour de owner page insta', res.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err owner page instagram", err))
    };
};

/* FACEBOOK */
export const connectFacebookPage = (idPage) => {
    return (dispatch) => {
        
        let page ={
            id: idPage,
        };

        return axios.post(host + `/api/v1/secure/pages/connect/`,page,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('retour de connect page facebook', res.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err de connect page facebook", err))
    }
};

export const disconnectFacebookPage = (idPage) => {
    return (dispatch) => {

        let page = {
            id: idPage,
        };
        
        return axios.post(host + `/api/v1/secure/pages/disconnect/disconnect`,page,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('retour de disconnect page facebok', res.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err de disconnect page facebok", err))

    };
};

export const deleteFacebookPage = (idPage,idFacebook) => {
    return (dispatch) => {

        let page = {
            id: idPage,
        };
        
        return axios.post(host + `/api/v1/secure/pages/disconnect/delete`,page,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('retour de disconnect page facebok', res.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err de disconnect page facebok", err))

    };
};

export const makeOwnerFacebookPage = (idPage,idFacebook) => {
    return (dispatch) => {
     
        let page = {
            id: idPage,
            idFacebook: idFacebook // idFacebook of the new owner
        };
        
        return axios.post(host + `/api/v1/secure/pages/disconnect/owner`,page,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('retour de owner page facebook', res.data);
            
            // dispatch({
            //     type: 'GET_ENTITIES_OF_A_PAGE',
            //     payload: res.data.data
            // })
        })
        .catch((err) => console.log("err owner page facebook", err))
    };
};


export const selectSocialMediaPage = (page,platform,status) => {
    return async (dispatch) => {

        // console.log("Full page data =>", page );
        // let userData = JSON.parse(localStorage.getItem('userData'));

        // console.log("Full userData", userData.user.idFacebook);

        /*
            ALL STATUS :
            props.status === "listPageConnectedOwner" 
            ? 
            #E5007D" 
            : 
            props.status === "listPageInvite" 
            ?
            #139216"
            : 
            "listPageInviteAdmin" 
            ?
            #199EE3" 
            :
            #B4B4B4"
        */
 
        const selectedPage = {
            platform: platform === "fb" ? "facebook" : "instagram",
            name: page.name,
            picture_url : page.imageUrl ? page.imageUrl : `https://graph.facebook.com/v8.0/${page.idPage}/picture?access_token=${page.access_token}`,
            id: page.idPage,
            accessToken: page.access_token,
            team: page.user,
            admins: page.admins,
            status: status,
        };
        
        dispatch({
            type: 'SELECT_SOCIAL_MEDIA_PAGE',
            payload: selectedPage
        })
    }
};

export const getFbData = () => {
    return async (dispatch) => {
        
        /*
            platform={"fb"} 
            isConnectedPage={page.isConnected}
            namePage={page.name} 
            idPage={page.idPage} 
            picture={null} 
            accessToken={page.access_token} 
            status={key} 
            team={page.user} 
            admins={page.admins}
        */
        
        let token = await localStorage.getItem('authtoken');

        return axios.get(host + '/api/v1/secure/pages/get/all/', {
            headers: {
                'authorization': 'Bearer ' + token
            }
        })
        .then(res => {
        
            let allFbPages = [];
            Object.entries(res.data.data).map( ([key, tab]) => 
                tab.map((page) => {
                    page.status = key;
                    allFbPages.push(page);
            }));

            allFbPages.sort((page,nextPage) =>  {
                let  nameA = page.name.toLowerCase(), nameB = nextPage.name.toLowerCase()
                //sort ascending 
                if (nameA < nameB) return -1; 

                if (nameA > nameB) return 1;
                
                return 0; // default return value (no sorting)
            });

            dispatch({
                type: 'GET_FB_DATA',
                payload: allFbPages,
                payload2: res.data.data,
            })
        })
        .catch((err) => {
            console.log("GetFbData err", JSON.stringify(err))
        })
    }
};

export const getInstaData = () => {
    return async (dispatch) => {

        /*
            platform={"fb"} 
            isConnectedPage={page.isConnected}
            namePage={page.name} 
            idPage={page.idPage} 
            picture={null} 
            accessToken={page.access_token} 
            status={key} 
            team={page.user} 
            admins={page.admins}
        */
       let token = await localStorage.getItem('authtoken');
        
        return axios.get(host + '/api/v1/secure/pagesInsta/get/all/', {
            headers: {
                'authorization': 'Bearer ' + token
            }
        })
        .then(res => {
            // console.log('Instagram pages ya haider =>', res.data.data)
            let allInstaPages = [];
            
            Object.entries(res.data.data).map( ([key, tab]) => 
                tab.map((page) => {
                    page.status = key;
                    allInstaPages.push(page);
                }
            ));

            allInstaPages.sort((page,nextPage) =>  {                
                let  nameA = page.name.toLowerCase(), nameB = nextPage.name.toLowerCase()
                //sort ascending 
                if (nameA < nameB) return -1; 

                if (nameA > nameB) return 1;
                
                return 0; // default return value (no sorting)
            });
            
            dispatch({
                type: 'GET_INSTA_DATA',
                payload: allInstaPages,
                payload2: res.data.data
            })
        })
        .catch((err) => {
            console.log("getInstaData error", JSON.stringify(err))
        })
    }
};

export const resetSocialMediaSelections = () => {
    return async (dispatch) => dispatch({ type: 'RESET_SM_SELECTIONS' });
};


