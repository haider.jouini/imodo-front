const initialState = {
    fbData: null,
    fbDataFiltered: null,
    instaData: null,
    instaDataFiltered: null,
    socialMediaPageSelected: null,
    PageEntities: null,
}

// renderPage(index,"fb",page.name,null,page.idPage,page.access_token,"listPageInvite")   
// {renderPage(index,"insta",page.name,page.imageUrl,page.idPage,page.access_token,"listPageInvite")}

const socialMediaReducer = (state = initialState, action) => {

    switch (action.type) {
        
        case 'GET_ENTITIES_OF_A_PAGE' :
            return {
                ...state,
                PageEntities : action.payload,
            }
        
        case 'SELECT_SOCIAL_MEDIA_PAGE' :
            return {
                ...state,
                socialMediaPageSelected : action.payload,
            }

        case 'RESET_SM_SELECTIONS' :
            return {
                ...state,
                socialMediaPageSelected : null,
                PageEntities : null,
            }

        case 'GET_FB_DATA' :
            return {
                ...state,
                fbData : action.payload,
                fbDataFiltered: action.payload2,
            }

        case 'GET_INSTA_DATA' :
            return {
                ...state,
                instaData : action.payload,
                instaDataFiltered : action.payload2,
            }

        default:
            return state;
    }
}

export default socialMediaReducer;

