import { combineReducers } from 'redux';
import socialMediaReducer from './socialMediaReducer';
import wizardReducer from './wizardReducer';

const rootReducer = combineReducers({
   socialMediaR : socialMediaReducer,
   wizardR : wizardReducer,
});

export default rootReducer;