import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    // Redirect,
} from "react-router-dom";

import Home from './app/Home/Home';
import LandingPage from './app/Login/LandingPage';


function App() {
    
    return (
        <Router>
            <Switch>
                <Route path="/home">
                    <Home />
                </Route>
                
                <Route path="/:tokenMembership?">
                    <LandingPage />
                </Route>
            
            </Switch>

        </Router>
    );
}

export default App;

