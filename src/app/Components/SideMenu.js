import React from 'react'
import './Components.css';

import {
    Link
} from 'react-router-dom';

import {
    Col,
    Row,
    Image,
    Nav,
} from 'react-bootstrap';

// Images & icons :
const BULLE_IMODO = require('../../assets/images/home/bulle-imodo.png');
const HOME_ICON = require('../../assets/images/home/home-icon.png');
const HOME_ICON_ACTIVE = require('../../assets/images/home/home-icon-active.png');
const AGENTS_ICON = require('../../assets/images/home/agents-icon.png');
const AGENTS_ICON_ACTIVE = require('../../assets/images/home/agents-icon-active.png');
const PRODUCTS_ICON = require('../../assets/images/home/products-icon.png');
const PRODUCTS_ICON_ACTIVE = require('../../assets/images/home/products-icon-active.png');
const TEAM_ICON = require('../../assets/images/home/team-icon.png');
const TEAM_ICON_ACTIVE = require('../../assets/images/home/team-icon-active.png');



function SideMenu() {

    const sideMenuItems = [
        {title: 'My pages', img: HOME_ICON, imgActive: HOME_ICON_ACTIVE, route: 'pages'},
        {title: 'My agents', img: AGENTS_ICON, imgActive: AGENTS_ICON_ACTIVE, route: 'my-agents'},
        {title: 'My products', img: PRODUCTS_ICON, imgActive: PRODUCTS_ICON_ACTIVE, route: 'manage-products'},
        {title: 'Team', img: TEAM_ICON, imgActive: TEAM_ICON_ACTIVE, route: 'team'},
    ];


    return (
        <>
            <Col lg={12} className="m-auto">
                <Link to="/home/wizard-popup" style={{ textDecoration: 'none' }}>
                    <div className="side-menu-wizard-btn d-flex p-2">
                        <Row className={"m-auto"}> 
                            <Col lg={4} className="d-flex">
                                <Image src={BULLE_IMODO} className="m-auto" />
                            </Col>
                            
                            <Col className="d-flex">
                                <p className="side-menu-wizard-text m-auto">Auto-moderation<br />wizard</p>
                            </Col>
                        </Row>
                    </div>
                </Link>
            </Col>

            <div style={{height: '80px'}}></div>

            <Col lg={12} className="m-auto d-flex">
            
                <Nav 
                    defaultActiveKey="/home" 
                    className="flex-column"
                    // onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
                >
                    {
                        sideMenuItems.map((menuItem,index) =>
                            <Link to={`/home/${menuItem.route}`} key={index} className="mt-2 mb-2" style={{ textDecoration: 'none' }}>
                                <Row className="d-flex">
                                    <Col lg={3} className="d-flex">
                                        <Image src={menuItem.img} fluid className="m-auto" />
                                    </Col>
                                    <Col lg={9} className="d-flex">
                                        <p className="mr-auto my-auto side-menu-title">{menuItem.title}</p>
                                    </Col>
                                </Row>
                            </Link>    
                        )
                    }
                </Nav>
        
            </Col>

        </>
    )
}

export default SideMenu;
