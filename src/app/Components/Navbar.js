import React, { useEffect, useState } from 'react'
import './Components.css';

import Facebook from '../Login/Facebook';

import {
    Row,
    Col,
    Nav,
    Image,
    Spinner,
    NavItem,
    Dropdown
} from 'react-bootstrap';
import axios from 'axios';

import {
    Redirect
} from 'react-router-dom';

import { host } from '../../config';

import { IoIosArrowDown } from 'react-icons/io';

const LOGO = require('../../assets/images/login/logo.png');

/*
{user: {…}, token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1O…1NTF9.vVGCbgia5UAWUb5O_ti6vmuDiYaYrYl6KTVcURBSt7k"}
token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTk3MzE1NTF9.vVGCbgia5UAWUb5O_ti6vmuDiYaYrYl6KTVcURBSt7k"
user:
access_token: "EAALJB1gyZAkYBAL1SK54ZAelZAptQUwHV89Vb0ZALIYWlz2DuD5o7NNgBDBloPIpcD0V6ZBWuVtvgKJbBmx4DOuZBL4nVYRk83bb0J8aTXZC4aYX5qdI8fdeAD0pkwHWNC0fRRHJZBgS68smkfQDZCssibBPfo46M7EQnBzFWXg5QRgZDZD"
create_at: "2020-08-04T12:43:40.410Z"
entities: []
first_name: "Farid"
idFacebook: "206455304112087"
isConnected: true
last_name: "Sassi"
last_signin: "2020-09-10T09:52:31.315Z"
pages: (7) [{…}, {…}, {…}, {…}, {…}, {…}, {…}]
parent_id: []
profilePictureUrl: "https://scontent.ftun5-1.fna.fbcdn.net/v/t1.30497-1/c212.0.720.720a/p720x720/84628273_176159830277856_972693363922829312_n.jpg?_nc_cat=1&_nc_sid=12b3be&_nc_ohc=4RSf-Cvnc8kAX_I4ilU&_nc_oc=AQm-zePN4JNrOCo9VrDGlK9-EYUNBXpqIbJ0qqdC3FOHTIc3jD0FzdCXyvMSCyeIUcA&_nc_ht=scontent.ftun5-1.fna&oh=8667c2565c91557a63e677f37e1216d6&oe=5F4D909F"
projects: (14) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]

*/

function Navbar(props) {
    
    const [userDataFromLogin, setUserDataFromLogin] = useState(null);
    const [redirect, setRedirect] = useState(false);
    
    useEffect(() => {
        let userDataFromLocal = JSON.parse(localStorage.getItem('userData'));
        setUserDataFromLogin(userDataFromLocal);
    },[]);

    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <a href="" ref={ref} onClick={(e) => { e.preventDefault(); onClick(e); }} >
          <IoIosArrowDown color={"#818E94"} size={"17"} className="my-auto ml-2 navbar-arrow-icon" />
        </a>
    ));

    const Logout = async () => {
        
        setUserDataFromLogin(null);

        await axios.post(host + '/api/v1/auth/logout', null, {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        }).then(async (res) => {
            console.log("Rep logout", res.data);
            await localStorage.clear();
            setTimeout(() => {
                setRedirect(true);
            },1000);
        })
        .catch(err => {
            console.log("Cannot logout", err);
        });

    };
      
    return (
        <>
        { redirect && <Redirect to={"/"} /> }
        <Nav
            activeKey="/home"
            onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
            // className="justify-content-between mx-0 pt-0"
            style={{
                backgroundColor: 'white',
                boxShadow: "0px 3px 6px rgba(0, 0, 0, 0.16)",
            }}
        >
            {/* <Col lg={1} /> */}

                <Col lg={6}>
                    <Row>
                        <Col className="d-flex" lg={4}>
                            <Image src={LOGO} className={"m-auto p-2"} />
                        </Col>

                        <Col lg={8}  className="d-flex">
                            <Row className={"my-auto mr-auto p-2"}>
                                {
                                    props.menuItems.length > 0 
                                    && 
                                    props.menuItems.map(((menuItem,index) => 
                                        <Nav.Item key={index}>
                                            <Nav.Link href={menuItem.route} className="navbar-menu-title">{menuItem.title}</Nav.Link>
                                        </Nav.Item>
                                    ))
                                }
                            </Row>
                        </Col>
                    </Row>
                </Col>

                <Col lg={5} className="d-flex">
                    <Row className={"my-auto ml-auto p-3"}>
                        {
                            props.loginBtn
                            ?
                            <Nav.Item>
                                <Facebook content="Login" />
                            </Nav.Item>
                            :
                            userDataFromLogin && !props.loginBtn
                            ?
                            <Nav.Item>
                                <Row className="d-flex">
                                    <Image src={userDataFromLogin['user'].profilePictureUrl} style={{ maxWidth: '50px', borderRadius: '50%' }} className={"my-auto"} />
                                    <Dropdown as={NavItem}>
                                        <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components" />

                                        <Dropdown.Menu>
                                            <Dropdown.Item onClick={() => Logout()} className="navbar-menu-title">Logout</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </Row>
                            </Nav.Item>
                            :
                            <Spinner animation="border" size="sm" variant="dark" />
                        }
                    </Row>
                </Col>
            
            <Col lg={1} />

        </Nav>

        </>
    );
}


export default Navbar;