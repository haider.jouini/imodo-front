import React, { useState, useRef,useEffect } from 'react';
import { connect } from 'react-redux';

import AddSynonymInput from './AddSynonymInput';
import { FaTrashAlt } from 'react-icons/fa';
import { FiEdit } from 'react-icons/fi';
import { AiOutlineCloseCircle } from 'react-icons/ai';
import ReactTooltip from 'react-tooltip';

import {
    Col,
    Row,
} from 'react-bootstrap';

export const ProductDetails = (props) => {

    const { product,index,addSynonym,allProducts } = props;
    const inputRef = useRef();
    const [modifyProductContent, setModifyProductContent] = useState(allProducts[index].content);
    const [enableProductInput, setEnableProductInput] = useState(false);
    const [synonymBoxBorder, setSynonymBoxBorder] = useState(false);


    const modifyProduct = () => {
        let { allProducts, index } = props;
        allProducts[index].content = modifyProductContent;
        setEnableProductInput(false);
    };

    const deleteProduct = () => {
        let { allProducts,setAllProducts } = props;
        let oldProducts = [...allProducts];

        oldProducts.splice(index,1);
        setAllProducts(oldProducts);    
    };

    const deleteSynonym = (indexProduct,indexSyn) => {
        let { allProducts,setAllProducts } = props;
        let oldProducts = [...allProducts];

        oldProducts[indexProduct].synonyms.splice(indexSyn,1);
        setAllProducts(oldProducts);
    }

    useEffect(() => {
        enableProductInput && inputRef.current.focus();
    },[enableProductInput]);

    return (
    <Row className="p-3" key={index} style={{ borderBottom: '1px solid #EBEDF0' }}>
        <Col lg={5} className="d-flex">
            <div className="mr-auto d-flex">
                {enableProductInput
                ?
                <input 
                    type="text" 
                    ref={inputRef} 
                    value={modifyProductContent}
                    onChange={(e) => setModifyProductContent(e.target.value)}
                    className="my-auto synonym-box-white w-100" 
                    style={{ border: '1px solid #818E94',fontSize: '15px', fontFamily: 'Poppins SemiBold', color: '#818E94' }}
                    onKeyDown={(e) => { if(e.key === 'Enter') modifyProduct(); }}
                />
                :
                <p className="my-auto manage-product-entity-name">{product.content}</p>
                }
            </div>
        </Col>

        <Col lg={7}>
            <Row>
                <Col lg={10}>
                    <Row className="mx-0">
                        {product.synonyms.length > 0
                        &&
                        product.synonyms.map((synonym,indexSyn) => 
                            <Col key={indexSyn} lg={5} className="synonym-box-gray d-flex mr-1 mb-2">
                                <Row className="d-flex mx-auto">
                                    {
                                        synonym.length > 11
                                        ?
                                        <p data-for='synonymTip' data-tip={synonym} className="my-auto" style={{cursor: 'help'}}>{synonym.substring(0,10)}...</p>
                                        :
                                        <p className="my-auto">{synonym}</p>
                                    }
                                    <AiOutlineCloseCircle 
                                        className="my-auto ml-1" 
                                        color={'#818E94'} 
                                        size={'17'} 
                                        style={{ cursor: 'pointer' }} 
                                        onClick={() => deleteSynonym(index,indexSyn)}
                                    />
                                </Row>
                                <ReactTooltip id='synonymTip' textColor='#fff' backgroundColor='#E5007D' />
                            </Col>    
                        )}

                        <Col lg={4} className="synonym-box-white d-flex mr-1 mb-2" style={{border: synonymBoxBorder ? '1px solid #818E94' : '1px dashed #818E94'}} onClick={() => setSynonymBoxBorder(true)}>
                            <AddSynonymInput addSynonym={addSynonym} index={index} product={product} setSynonymBoxBorder={setSynonymBoxBorder} />
                        </Col>
                    </Row>
                </Col>

                <Col lg={2} className="d-flex">
                    <Row className="ml-auto">
                        <FiEdit className="my-auto" color={'#E5007D'} size={'25'} style={{ cursor: 'pointer' }} onClick={ () => setEnableProductInput(true) }/>
                        <FaTrashAlt className="my-auto ml-3" color={'#E5007D'} size={'25'} style={{ cursor: 'pointer' }} onClick={ () => deleteProduct() }/>
                    </Row>
                </Col>
            </Row>
        </Col>
    </Row> 
    );
};

const mapStateToProps = (state) => ({
    
});

export default connect(mapStateToProps, {})(ProductDetails)
