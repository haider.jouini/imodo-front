import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux';

import ProductDetails from './ProductDetails';

import {
    Col,
    Row,
    Image,
    Spinner
} from 'react-bootstrap';

import {
    withRouter
} from 'react-router-dom';

import { FaInfoCircle } from 'react-icons/fa';
import { MdModeEdit } from 'react-icons/md';

const INSTA_ICON = require('../../../assets/images/home/insta-icon.png');
const FB_ICON = require('../../../assets/images/home/fb-icon.png');


export const AddNewProducts = (props) => {

    const [showPopover, setShowPopover]  = useState(false);
    const [categoryName,setCategoryName] = useState('');
    const [categoryDone,setCategoryDone] = useState(false);
    const [newProduct,setNewProduct] = useState('');
    const [btnLoading,setBtnLoading] = useState(false);
    const [allProducts, setAllProducts] = useState([]);

    const { history } = props;

    const handlePopover = (delay) => {
        setTimeout( () => {
          setShowPopover(!showPopover);
        },delay)
    };

    const renderPopover = () => (
        <div className="d-flex manage-products-popover">
          <p className="m-auto">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
          </p>
        </div>
    );

    const createProducts = () => {    
        let oldProducts = [...allProducts];
        let valueToPush = {
            content: newProduct,
            synonyms: [],
        };

        oldProducts.push(valueToPush);
        setAllProducts(oldProducts);
        setNewProduct("");
    };

    const addSynonym = (indexToPush,valueToPush) => {    
        let oldProducts = [...allProducts];
        oldProducts[indexToPush].synonyms.push(valueToPush);
        setAllProducts(oldProducts);
    };

    const prepareEntitiesAndSave = () => {
        setBtnLoading(true);

        setTimeout(() => 
            setBtnLoading(false)
        ,1500);

        let entity = {
                name: categoryName,
                values: allProducts,
        };

        console.log("U will send", entity);
    };

    useEffect(() => {
       if(!props.socialMediaPageSelected) {
           history.push('manage-products');
       };
    }, []);

    return (
    <div style={{height: window.innerHeight * 1.2}} className="d-flex flex-column">
           
        <Row className="mt-5 mr-auto">
        {categoryName && categoryDone
        ?
        ( <p className="my-agents-table-label" style={{ fontSize: '30px', fontFamily: 'Poppins SemiBold' }}>Update category</p> )
        :
        ( <p className="my-agents-table-label" style={{ fontSize: '30px', fontFamily: 'Poppins SemiBold' }}>Add category</p> )
        }
        <FaInfoCircle color={"#4080FF"} size={'15'} onMouseEnter={() => handlePopover('20')} onMouseLeave={() => handlePopover('300')} />
        { showPopover && renderPopover() }
        </Row>

        {/* SELECTED PAGE */}
        {
        props.socialMediaPageSelected &&
        <Row className="mr-auto d-flex mb-3">
            <Row className="wizard-selected-page-container m-auto" style={{backgroundColor: 'transparent'}}>
                <div className="d-flex flex-column mr-3">
                    <Image src={props.socialMediaPageSelected.picture_url} className="m-auto" style={{ maxWidth: '40px' }} />
                    <Image src={props.socialMediaPageSelected.platform === "instagram" ? INSTA_ICON : FB_ICON} className="ml-auto" style={{zIndex: 1, marginTop: '-10px'}}/>
                </div>
                <p className="m-auto wizard-pages-page-title">{props.socialMediaPageSelected.name}</p> 
            </Row>
        </Row>
        }
        
        {/* Category Name */}
        <Row className="d-flex">   
            {categoryName && categoryDone
            ?
                (
                <Col lg={5} className="p-0">
                    <div className="d-flex">
                        <p className="my-auto wizard-pages-page-title">{categoryName}</p>
                        <MdModeEdit className="my-auto ml-3" color={'#4B4F56'} size={'20'} onClick={() => setCategoryDone(false)} style={{ cursor: 'pointer' }} />
                    </div>
                </Col>
                )
            :
                (
                <Col lg={2} className="add-new-product-input-container">
                    <input type="text" className="add-new-product-input w-100" placeholder="Type category name"  value={categoryName} onChange={(e) => setCategoryName(e.target.value)} onKeyDown={(e) => e.key === 'Enter' && setCategoryDone(true)} />
                </Col>
                )
            }
            <Col lg={10} />
        </Row>
        
        {/* ADD PRODUCTS */}
        <Row className="d-flex mt-3">   
            <Col lg={3}>
                <Row className="justify-content-between">
                    <Col lg={6} className="add-new-product-input-container">
                        <input 
                            type="text" 
                            className="add-new-product-input w-100" 
                            disabled={categoryDone ? false : true} value={newProduct} 
                            onChange={(e) => setNewProduct(e.target.value)} 
                            onKeyDown={(e) => { if(e.key === 'Enter') setNewProduct(e.target.value) }}
                        />
                    </Col>

                    <Col lg={5}>
                        <Row className={categoryDone ? "manage-products-add-btn d-flex" : "manage-products-add-disabled-btn d-flex"} onClick={ () => categoryDone ? createProducts() : null }>
                            {/* <MdAddCircleOutline className="my-auto mr-1" color={iconColor} size={'20'} /> */}
                            <p className="m-auto">Add products</p>
                        </Row>
                    </Col>
                </Row>
            </Col>

            <Col lg={7} />
        </Row>
        
        {
            allProducts.length > 0
            &&
            <>
            <Row className="mt-5 mr-0">
                <Col lg={10} className="p-3" style={{backgroundColor: 'white', borderRadius: '0.3rem'}}> 
                    {
                    allProducts.length > 0
                    &&
                    <>
                    {/* TITLES */}
                    <Row className="p-3" style={{borderBottom: '1px solid #EBEDF0'}}>
                        <Col lg={5} className="d-flex">
                            <div className="mr-auto d-flex">
                                <p className="my-auto manage-products-table-title">Products</p>
                            </div>
                        </Col>

                        <Col lg={7}>
                            <div className="d-flex mr-auto">
                                <p className="mr-5 my-auto manage-products-table-title">Synonyms</p>
                            </div>
                        </Col>
                    </Row>

                    {/* LINE */}
                    {allProducts.map((product,index) => 
                        <ProductDetails addSynonym={addSynonym} product={product} index={index} allProducts={allProducts} setAllProducts={setAllProducts} />
                    )}
                    </>
                    }
                </Col>

                <Col lg={2} />
            </Row>

        {/* Buttons */}
        <Row className="mt-5 mr-0">
            <Col lg={10} className=""> 
                <Row className="justify-content-between">
                    <div className="wizard-pages-inactive-btn">
                        Back    
                    </div>

                    <div className="wizard-pages-active-btn" onClick={() => prepareEntitiesAndSave() }>
                        { btnLoading ? <Spinner animation="border" variant="light" /> : "Save"}
                    </div>
                </Row>
            </Col>

            <Col lg={2} />
        </Row>

        </>
        }
    </div>
    );
}

const mapStateToProps = (state) => {
    return {
      socialMediaPageSelected : state.socialMediaR.socialMediaPageSelected,
    }
};

export default withRouter(connect(mapStateToProps, {})(AddNewProducts));
