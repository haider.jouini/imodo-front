import React, { useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';

import {
    Row,
} from 'react-bootstrap';

import { MdAddCircleOutline } from 'react-icons/md';


export const AddSynonymInput = (props) => {

    const inputRef = useRef();
    const [enableSynonymInput,setEnableSynonymInput] = useState(false);
    const [Synonym,setSynonym] = useState('');

    useEffect(() => {
        enableSynonymInput && inputRef.current.focus();
    },[enableSynonymInput]);


    const newSynonym = () => {
        let { index } = props;

        props.addSynonym(index,Synonym); 
        setSynonym(''); 
        setEnableSynonymInput(false); 
        props.setSynonymBoxBorder(false); 
    };

    return (
        <div>
            {
            enableSynonymInput
            ?
            <Row className="d-flex mx-auto" style={{ cursor: 'pointer' }}>
                <input 
                    type="text" 
                    className="add-new-product-input w-100" 
                    ref={inputRef} 
                    value={Synonym} 
                    onChange={(e) => setSynonym(e.target.value)} 
                    onKeyDown={(e) => e.key === 'Enter' && newSynonym()} 
                />
            </Row>
            : 
            <Row className="d-flex mx-auto" style={{ cursor: 'pointer' }} onClick={() => { setEnableSynonymInput(true); }}>
                <MdAddCircleOutline className="my-auto mr-1" color={'#818E94'} size={'17'}  />
                <p className="my-auto">Add Synonym</p>
            </Row>
            }
        </div>
    )
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps,{})(AddSynonymInput)
