import React, { useState, useEffect } from 'react'

import { connect } from 'react-redux';
import { getPageEntities, resetSocialMediaSelections } from '../../../redux/actions/socialMediaActions';

import Pages from '../Pages/Pages';

import {
  Col,
  Row,
} from 'react-bootstrap';

import Lottie from 'react-lottie';

import { withRouter } from 'react-router-dom';

import { BiSearch } from 'react-icons/bi';
import { FaTrashAlt, FaInfoCircle } from 'react-icons/fa';
import { MdAddCircleOutline } from 'react-icons/md';
import animationData from '../../../assets/json/loading.json';
import notFoundAnimationData from '../../../assets/json/not-found.json';


function ManageProducts(props) {

    const [showPopover, setShowPopover] = useState(false);
    const [iconColor, setIconColor] = useState('#fff');
    const [loading, setLoading] = useState(false);

    const { history } = props;

    const defaultOptions = {
      loop: true,
      autoplay: true, 
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };

    const defaultOptionsNotFound = {
      loop: true,
      autoplay: true, 
      animationData: notFoundAnimationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };

    const handlePopover = (delay) => {
      setTimeout( () => {
        setShowPopover(!showPopover);
      },delay)
    };

    const handleAddIconColor = (color) => {
      setIconColor(color);
    };

    const renderPopover = () => (
      <div className="d-flex manage-products-popover">
        <p className="m-auto">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
        </p>
      </div>
    );

    useEffect(() => {
      props.resetSocialMediaSelections();
    },[]);

    useEffect(() => {
      if(props.PageEntities) {
        console.log("entities =>", props.PageEntities);
      }
    },[props.PageEntities]);

    useEffect(() => {
      if(props.socialMediaPageSelected) {
        setLoading(true);

        setTimeout(() => {
          props.getPageEntities(props.socialMediaPageSelected)
               .then(() => setLoading(false));
        },2000);
      };
    },[props.socialMediaPageSelected]);
    
    return (
      <div style={{height: window.innerHeight * 1.2}}>
        <Row className="mt-5">
          <p className="my-agents-table-label" style={{ fontSize: '30px', fontFamily: 'Poppins SemiBold' }}>Products management</p>
          <FaInfoCircle color={"#4080FF"} size={'15'} onMouseEnter={() => handlePopover('20')} onMouseLeave={() => handlePopover('300')} />
          { showPopover && renderPopover() }
        </Row>

        <Pages title="Select a page to manage products" titleStyling={{color: '#4D4F5C', fontSize: '16px', fontFamily: 'Poppins Medium' }} />

        {
          props.socialMediaPageSelected &&
          <>
          {
            !loading
            ?
              (
                <>
                {/* ADD & SEARCH */}
                <Row className="mt-5 d-flex mr-0">
      
                  <Col lg={10}>
                    <Row className="mr-0">
                      <Row 
                        className="manage-products-add-btn d-flex mx-0 mr-auto" 
                        onMouseEnter={() => handleAddIconColor('#E5007D')} 
                        onMouseLeave={() => handleAddIconColor('#fff')}
                        onClick={ () => props.socialMediaPageSelected && history.push('add-new-products') } 
                      >
                        <MdAddCircleOutline className="my-auto mr-1" color={iconColor} size={'20'} />
                        <p className="my-auto">Add category</p>
                      </Row>
      
                      <div className="manage-products-search-container pr-4 pl-4 ml-auto">
                        <Row className="d-flex">
                          <input type={"text"} className="my-auto mr-2 manage-products-search-input" />
                          <BiSearch color={"#B4B4B4"} size={'20'} className="my-auto" />
                        </Row>
                      </div>
                    </Row>
                  </Col>
      
                  <Col lg={2} />
                </Row>
      
                {/* ENTITIES TABLE */}
                <Row className="mt-5 mr-0">
                  {
                    props.PageEntities !== null && props.PageEntities.length > 0
                    ?
                      (
                        <>
                          <Col lg={10} className="p-3" style={{backgroundColor: 'white', borderRadius: '0.3rem'}}>
                            <div className="d-flex mx-0 w-100 pt-3 pb-3" style={{borderBottom: '1px solid #EBEDF0'}}>
                              <div className="mr-auto d-flex">
                                <p className="my-auto manage-products-table-title">Category</p>
                              </div>
            
                              <div className="d-flex ml-auto">
                                <p className="mr-5 my-auto manage-products-table-title">Products</p>
                                <div className="ml-5 my-auto" />
                              </div>
                            </div>

                            {
                              props.PageEntities.map( (entity,index) => 
                                <div className="d-flex mx-0 w-100 pt-3 pb-3 pr-2 pl-2" style={{borderBottom: '1px solid #EBEDF0'}}>
                                  <div className="mr-auto d-flex">
                                    <p className="my-auto manage-product-entity-name">{entity.name}</p>
                                  </div>

                                  <div className="d-flex ml-auto">
                                    <p className="mr-5 my-auto manage-product-entity-name">{entity.children.length}</p>
                                    <FaTrashAlt className="ml-5 my-auto" color={'#E5007D'} size={'25'} />
                                  </div>
                                </div>
                              )
                            }
                          </Col>
                        </>
                      )
                      :
                      (
                        <>
                        <Col lg={12} className="d-flex" style={{backgroundColor: '#F9F9F9' }}>
                          <Lottie options={defaultOptionsNotFound} width={200} className="m-auto" /*height={400}*/ />
                        </Col>

                        <Col lg={12} className="d-flex mt-5" style={{backgroundColor: '#F9F9F9' }}>
                            <p className="m-auto home-big-title">No entities for your page ! add a new one ?</p>
                        </Col>
                        </>
                      )
                  }

                  <Col lg={2} />
                </Row>
                </>
              )
            :
              (
                <div className="d-flex" style={{ width: '100%' }}>
                    <div className="m-auto">
                        <Lottie options={defaultOptions} width={200} /*height={400}*/ />
                    </div>
                </div>
              )
          }
          </>
        }
      </div>
    )
}

const mapStateToProps = (state) => {
  return {
    socialMediaPageSelected : state.socialMediaR.socialMediaPageSelected,
    PageEntities : state.socialMediaR.PageEntities,
  }
};

export default withRouter(connect(mapStateToProps, { getPageEntities, resetSocialMediaSelections })(ManageProducts));
