import React, { useEffect, useState } from 'react'
import '../Home.css';
import {
    Row,
    Col,
    Dropdown
} from 'react-bootstrap';
import OwlCarousel from 'react-owl-carousel';
import Slider from "react-slick";
import Lottie from 'react-lottie';

import { connect } from 'react-redux';
import { selectSocialMediaPage, getFbData, getInstaData } from '../../../redux/actions/socialMediaActions';

import PageCard from './PageCard';

import animationData from '../../../assets/json/loading.json';
import { RiListSettingsFill } from 'react-icons/ri';
import { IoIosArrowDown } from 'react-icons/io';


function Pages(props) {

    const [loadingPages, setLoadingPages] = useState(true);
    const [showFacebook, setShowFacebook] = useState(true);
    const [showInstagram, setShowInstagram] = useState(true);
    const [resetCarousel, setResetCarousel] = useState(0);

    const defaultOptions = {
        loop: true,
        autoplay: true, 
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
    };

    const settings = {
        dots: true,
        infinite: false,
        speed: 700,
        slidesToShow: 3.7,
        slidesToScroll: 3,
        initialSlide: 0,
        slickGoTo: resetCarousel,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
    };

    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <a  className="pages-filter-box d-flex py-1 px-3" ref={ref} onClick={(e) => {e.preventDefault(); onClick(e);}} href="" style={{ textDecoration: 'none'}}>
            <RiListSettingsFill className={"my-auto mr-2"} color={"#B4B4B4"} size={'25'} /> 
            <p className={"m-auto"}>Filter By page</p>
            <span className={"m-auto"}>
                <IoIosArrowDown className={"my-auto ml-3"} color={"#B4B4B4"} size={'25'} /> 
            </span>
        </a>
    ));

    const CustomMenu = React.forwardRef(
        ({ children, style, className, 'aria-labelledby': labeledBy }, ref) => {
        
          return (
            <div
              ref={ref}
              style={style}
              className={className}
              aria-labelledby={labeledBy}
            >
                <Col lg={12} className="d-flex filter-item p-3" onClick={() => { setShowFacebook(true); setShowInstagram(true); setResetCarousel(0)}}>
                    <p className="ml-2 mr-auto my-auto">ََAll</p>
                </Col>

                <Col lg={12} className="d-flex filter-item p-3" onClick={() => { setShowFacebook(true); setShowInstagram(false); setResetCarousel(0) }}>
                    <p className="ml-2 mr-auto my-auto">Facebook</p>
                </Col>

                <Col lg={12} className="d-flex filter-item p-3" onClick={() => {  setShowInstagram(true); setShowFacebook(false); setResetCarousel(0)}}>
                    <p className="ml-2 mr-auto my-auto">Instagram</p>
                </Col>
            </div>
          );
        },
      );

    useEffect(() => {
        props.getFbData();
        props.getInstaData();
    },[]);


    useEffect(() => {
        if(props.fbData) {
            setTimeout(() => {
                setLoadingPages(false);
            },1000);
            // console.log("Pages fb data =>", props.fbData);
        };
        return () => {
            // removing the listener when props.x changes
        }
    },[props.fbData]);

    useEffect(() => {
        if(props.instaData) {
            setTimeout(() => {
                setLoadingPages(false);
            },1000);
            // console.log("Pages insta Data =>", props.instaData);

        };
        return () => {
            // removing the listener when props.x changes
        }
    },[props.instaData]);
    
    return (
    <>
        <Row>
            <Col lg={6} className="d-flex">
                {
                props.title 
                ?
                    <p className="mr-auto home-big-title" style={props.titleStyling ? props.titleStyling : {} }>{props.title}</p>
                :
                    <p className="mr-auto home-big-title">Pages Connected</p>
                }
            </Col>

            <Col lg={5} className="d-flex">
                <div className="ml-auto">
                    <Dropdown>
                        <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components"/>
                            {/* <RiListSettingsFill color={"#B4B4B4"} size={'25'} /> Filter By page
                        </Dropdown.Toggle> */}

                        <Dropdown.Menu as={CustomMenu} />
                        
                    </Dropdown>
                </div>
            </Col>

            <Col lg={1} />
            
        </Row>

        <Row>

            {
            !loadingPages
            ?
                (
                    <>

                    {props.fbData && props.instaData && props.fbData.length + props.instaData.length > 3 
                    ?
                    (
                    <div style={{ width: '95%', height: '400px' }}>
                        <Slider {...settings}>
                            {
                            props.fbData !== null && showFacebook &&
                                props.fbData.map((page,index) => 
                                    <div key={index} className="p-3" onClick={() => props.selectSocialMediaPage(page,"fb",page.status)}>
                                        <PageCard 
                                            platform={"fb"}
                                            isConnectedPage={page.isConnected} 
                                            namePage={page.name} 
                                            idPage={page.idPage} 
                                            picture={null} 
                                            accessToken={page.access_token} 
                                            status={page.status} 
                                            team={page.user} 
                                            admins={page.admins} 
                                            setLoadingPages={setLoadingPages} 
                                        />
                                    </div>
                                )
                            }
                            
                            {
                            props.instaData !== null && showInstagram &&
                                props.instaData.map((page,index) => 
                                    <div key={index} className="p-3" onClick={() => props.selectSocialMediaPage(page,"insta",page.status)}>
                                        <PageCard 
                                            platform={"insta"} 
                                            isConnectedPage={page.isConnected}
                                            namePage={page.name} 
                                            idPage={page.idPage} 
                                            picture={page.imageUrl} 
                                            accessToken={page.access_token} 
                                            status={page.status} 
                                            team={page.user} 
                                            admins={page.admins} 
                                            setLoadingPages={setLoadingPages}
                                        />
                                    </div>
                                )
                            }
                        </Slider>
                    </div>
                    )
                    :
                    (
                    <Row className="d-flex">
                        {
                        props.fbData !== null &&
                            props.fbData.map((page,index) => 
                                <div key={index} className="p-3" onClick={() => props.selectSocialMediaPage(page,"fb",page.status)}>
                                    <PageCard 
                                        platform={"fb"}
                                        isConnectedPage={page.isConnected} 
                                        namePage={page.name} 
                                        idPage={page.idPage} 
                                        picture={null} 
                                        accessToken={page.access_token} 
                                        status={page.status} 
                                        team={page.user} 
                                        admins={page.admins} 
                                        setLoadingPages={setLoadingPages} 
                                    />
                                </div>
                            )
                        }
                            
                        {
                        props.instaData !== null &&
                            props.instaData.map((page,index) => 
                                <div key={index} className="p-3" onClick={() => props.selectSocialMediaPage(page,"insta",page.status)}>
                                    <PageCard 
                                        platform={"insta"} 
                                        isConnectedPage={page.isConnected}
                                        namePage={page.name} 
                                        idPage={page.idPage} 
                                        picture={page.imageUrl} 
                                        accessToken={page.access_token} 
                                        status={page.status} 
                                        team={page.user} 
                                        admins={page.admins} 
                                        setLoadingPages={setLoadingPages}
                                    />
                                </div>
                            )
                        }
                    </Row>
                    )

                    }
                    </>
                )
            :
                (
                <div className="d-flex" style={{ width: '100%' }}>
                    <div className="m-auto">
                        <Lottie options={defaultOptions} width={200} />
                    </div>
                </div>
                )
            }
        </Row>
    </>
    );
}

const mapStateToProps = (state) => {
    return {
        fbData : state.socialMediaR.fbData,
        instaData : state.socialMediaR.instaData,
        socialMediaPageSelected : state.socialMediaR.socialMediaPageSelected,
    }
};

export default connect(mapStateToProps, { selectSocialMediaPage,getFbData, getInstaData })(Pages);
