import React from 'react'

export default function OwnerCheckbox(props) {
    return (
        <>
          {props.checked 
            ?
                <div className={`page-card-owner-checkbox ${props.className} d-flex`} >
                    <div className="page-card-owner-checkbox-active m-auto"/>
                </div>
            :
                <div className={`page-card-owner-checkbox ${props.className}`} />


          }  
        </>
    )
}
