import React from 'react'
import './Home.css';

import Navbar from '../Components/Navbar';
import SideMenu from '../Components/SideMenu';
import Pages from './Pages/Pages';
import MyAgents from './MyAgents/MyAgents';
import MyAgentsMessages from './MyAgents/MyAgentsMessages';
import ManageProducts from './ManageProducts/ManageProducts';
import AddNewProducts from './ManageProducts/AddNewProducts';
import AutoWizard from './AutoWizard/AutoWizard';
import WizardPopup from './AutoWizard/WizardPopup';
import Team from './Team/Team';


import { connect } from 'react-redux';
import { getFbData, getInstaData } from '../../redux/actions/socialMediaActions';

import {
    Row,
    Col,
    Container,
} from 'react-bootstrap';

import {
    // BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    // useLocation, useHistory, useRouteMatch,
} from "react-router-dom";



function Home(props) {

    const auth_token = localStorage.getItem('authtoken');
    const userData = JSON.parse(localStorage.getItem('userData'));

    const allMenuItems = [
        { title: 'News' , route: 'link-what-is' },
        { title: 'How to use it' , route: 'link-how-to-use' },
        { title: 'Documentation' , route: 'link-for-devs' }
    ];

    return (
        <>
        { auth_token === null && userData === null && <Redirect to="/" /> }
        <Navbar menuItems={allMenuItems} userData={1} />
        <Container fluid>
            
        <Row>
            <Col lg={2} style={{background: 'white'}} className="pt-5">
                <SideMenu  />
            </Col>

            <Col lg={10} style={{background: '#F9F9F9',}} className="pt-5 pl-5">
                
                <Switch> 
                    <Route path="/home/pages">
                        <div style={{ height: window.innerHeight * 1.2}}>
                            <Pages />
                        </div>
                    </Route>

                    <Route path="/home/my-agents">
                        <MyAgents />
                    </Route>

                    <Route path="/home/my-agents-messages">
                        <MyAgentsMessages />
                    </Route>
                    
                    <Route path="/home/manage-products">
                        <ManageProducts />
                    </Route>

                    <Route path="/home/add-new-products">
                        <AddNewProducts />
                    </Route>

                    <Route path="/home/wizard-popup">
                        <WizardPopup />
                    </Route>

                    <Route path="/home/auto-wizard">
                        <AutoWizard />
                    </Route>

                    <Route path="/home/team">
                        <Team />
                    </Route>

                </Switch>
            </Col>
        </Row>
        </Container>
        </>
    )
};



const mapStateToProps = (state) => {
    return {
        fbData : state.socialMediaR.fbData,
        instaData : state.socialMediaR.instaData,
    }
};

export default connect(mapStateToProps, { getFbData, getInstaData })(Home);
