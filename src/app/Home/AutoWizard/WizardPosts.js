import React, { useEffect } from 'react'

import { connect } from 'react-redux';
import { nextWizardStep, getWizardPagesPosts, setWizardSelectedPost,prevWizardStep, addProject } from '../../../redux/actions/wizardActions';
import { getConnectedPagesProject } from '../../../redux/actions/socialMediaActions';

import {
    Row,
    Col,
    Image,
    Table,
    Spinner,
} from 'react-bootstrap';

import { Link } from 'react-router-dom';

import Lottie from 'react-lottie';
import Swal from 'sweetalert2/dist/sweetalert2.js';



import animationData from '../../../assets/json/loading.json';

import { FaLink } from 'react-icons/fa';
import { AiFillPicture } from 'react-icons/ai';

function WizardPosts(props) {
    
    const tableTitles = [
        {title : 'Post'},
        {title : 'Type'},
        {title : 'Published'},
        {title : 'Status'},
    ];

    // const [loading, setLoading] = useState(true);
   
    const defaultOptions = {
        loop: true,
        autoplay: true, 
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
    };

    useEffect(() => {
        props.getWizardPagesPosts(props.wizardSelectedPage);
    }, []);


    const checkNextStep = () => {

        if(props.wizardSelectedPost === null) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                  confirmButton: 'wizard-pages-active-btn-alert',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Please select a post',
                confirmButtonText: 'Okay',
            });
        }
    };

    const nextMove = (post) => {
        props.setWizardSelectedPost(post); 

        props.addProject(props.wizardSelectedPage,post)
        .then(() =>{
            props.nextWizardStep();
        });
        
    } 

    return (
    <Row className="d-flex mt-5">  
        <Col lg={1} />

        <Col lg={9} className="d-flex flex-column wizard-pages-container" style={{backgroundColor: 'white'}}>
            {/* Title */}
            <Row className="d-flex mt-4 ml-2">
                <p className="my-auto wizard-pages-title">Select one post to active the iModo moderation.</p>
            </Row>
            
            {/* POSTS LIST */}
            <Row  className="mt-5 mb-5">
                <Col lg={1} />

                <Col>
                    {props.wizardPagePosts
                    ?
                        (
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th></th>
                                    {tableTitles.map((item,index) => 
                                        <th key={index}>{item.title}</th>    
                                    )}
                                </tr>
                            </thead>
                            <tbody>
                            {
                                props.wizardPagePosts.map( (post,index) => {
                                    let createdTime = new Date(Date.parse(post.created_time));
                                    let publishedDate = `${createdTime.getDate()}/${createdTime.getMonth()+ 1}/${createdTime.getFullYear()}`;
                                    let checkedMinutes = createdTime.getMinutes() < 10 ? "0" + createdTime.getMinutes() : createdTime.getMinutes();
                                    let publishedTime = `${createdTime.getHours()}:${checkedMinutes}`
                                
                                    return (
                                        <tr key={index}  onClick={() => { nextMove(post) }} style={{ cursor: 'pointer' }}>
                                            <td></td>
                                            <td>
                                                <Row className="d-flex">
                                                    {
                                                        post.picture
                                                        ?
                                                        <Image src={post.picture} className="my-auto ml-3" style={{maxWidth: '50px'}} />
                                                        :
                                                        <Image src={props.wizardSelectedPage.picture_url} className="my-auto ml-3" style={{maxWidth: '50px'}} />
                                                    }
                                                    <p className="my-auto ml-2 wizard-pages-page-title" style={{cursor: 'pointer'}}>
                                                        {post.message && post.message.length > 40
                                                        ?
                                                        post.message.substring(0,37) + "..."
                                                        :                                                        
                                                        post.message
                                                        ?
                                                        post.message
                                                        :
                                                        props.wizardSelectedPage.name
                                                        }
                                                    </p>
                                                </Row>
                                            </td>

                                            <td>
                                                {
                                                post.picture
                                                ?
                                                <AiFillPicture color={"#B4B4B4"} size={"22"} className="mt-2 ml-2" />
                                                :
                                                <FaLink color={"#B4B4B4"} size={"22"} className="mt-2 ml-2" />
                                                }
                                            </td>

                                            <td>
                                                <div className="flex-column">
                                                    <p className="m-auto my-agent-project-date">{publishedDate}</p>
                                                    <p  className="m-auto my-agent-project-time">{publishedTime}</p>
                                                </div>
                                            </td>

                                            <td>
                                                <div className="d-flex">
                                                    {/* {props.wizardActivePagePosts
                                                    ?
                                                        (
                                                        isPostActive(post.id,props.wizardActivePagePosts) 
                                                        ? 
                                                        <p className="ml-3 wizard-posts-status-text" style={{color: '#E5137D'}}>Active</p>
                                                        : 
                                                        <p className="ml-3 wizard-posts-status-text">Not active</p>   
                                                        )
                                                        :
                                                        <Spinner animation="border" variant="dark" />
                                                    } */}
                                                    <p className="ml-3 wizard-posts-status-text">{post.type}</p>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                } 
                                )
                            }
                            </tbody>
                        </Table>
                        )
                    : 
                        (
                            <Row className="d-flex">
                                <div className="m-auto">
                                    <Lottie options={defaultOptions} width={200} />
                                </div>
                            </Row>
                        )
                    }
              
                </Col>

                <Col lg={1} />
            </Row>

            
            {/* BTNS */}
            <Col lg={12}>
                <Row className="mb-3">
                    <Col lg={6}>
                        <Row className="d-flex">
                            <div className="wizard-pages-inactive-btn d-flex py-2 px-4 ml-3 mr-auto my-auto" onClick={() => props.prevWizardStep()}>
                                Back
                            </div>
                        </Row>
                    </Col>

                    <Col lg={6}>
                        <Row className="d-flex">
                            <div className={"wizard-pages-inactive-btn ml-auto py-2 px-4 mr-3"}>Save Draft</div>
                            
                            <div className={"wizard-pages-active-btn py-2 px-4 mr-3"} onClick={() => checkNextStep()}>
                                Next
                            </div>
                        </Row>
                    </Col>
                </Row>
            </Col>

            {/* <Row className="d-flex justify-content-between pl-4 pr-4">
                <div className="wizard-pages-inactive-btn mb-3">
                    Back
                </div>

                <div className="mb-3">
                    <Row>
                        <div className="wizard-pages-inactive-btn mr-3">
                            Save Draft
                        </div>

                        <div className="wizard-pages-active-btn" onClick={() => props.nextWizardStep()}>
                            Next
                        </div>
                    </Row>
                </div>
            </Row> */}

        </Col>

        <Col lg={2} />
    </Row>
    );
}


const mapStateToProps = (state) => {
    return {
        wizardStep: state.wizardR.wizardStep,
        wizardSelectedPage: state.wizardR.wizardSelectedPage,
        wizardSelectedPost: state.wizardR.wizardSelectedPost,
        wizardPagePosts: state.wizardR.wizardPagePosts,
        wizardActivePagePosts: state.wizardR.wizardActivePagePosts,
    }
};

export default connect(mapStateToProps, { nextWizardStep, getWizardPagesPosts, getConnectedPagesProject, setWizardSelectedPost, prevWizardStep, addProject })(WizardPosts)
