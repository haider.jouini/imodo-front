import React, { useState, useRef, useEffect } from 'react';

import axios from 'axios';

import { connect } from 'react-redux';
import { addGenericResponse, updateGenericResponse } from '../../../redux/actions/wizardActions';
import { host } from '../../../config';

import {
    Row,
    Col,
    Popover,
    Overlay,
    Modal,
    Image,
    Form,
    Spinner,
} from 'react-bootstrap';
import WizardMiracleInput from './WizardMiracleInput';
import ReactTooltip from 'react-tooltip';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import { FaTrashAlt } from 'react-icons/fa';
import { IoIosArrowDown,IoIosClose } from 'react-icons/io';
import { AiOutlineCloseCircle } from 'react-icons/ai';
import { MdAddCircleOutline } from 'react-icons/md';

function WizardIntentDetails(props) {

    // All Props from WizardConfig : 
    let {
        indexOfIntent,
        intent,
        setIntents,
        selectedIntents,
        isExistingProject,
        intentStatus,
    } = props;
    
    // Popup Config:
    const swalWithBootstrapButtons = Swal.mixin({ customClass: {confirmButton: 'wizard-pages-active-btn-alert'}, buttonsStyling: false });
    
    // Generic:
    const [GenericResponse,setGenericResponse] = useState('');
    const [GenericTags,setGenericTags] = useState(false);
    const [GenericEmojis,setGenericEmojis] = useState(false);
    const [GenericKeyboard,setGenericKeyboard] = useState(false);
    const [GenericBtnType, setGenericBtnType] = useState('url');
    const [GenericBtnTitle, setGenericBtnTitle] = useState('');
    const [GenericBtnValue, setGenericBtnValue] = useState('');
    const [GenericBtns, setGenericBtns] = useState([]);
    const resetGeneric = () => {
        setGenericResponse('');
        setGenericTags(false);
        setGenericEmojis(false);
        setGenericKeyboard(false);
        setGenericBtnType('url');
        setGenericBtnTitle('');
        setGenericBtnValue('');
        setGenericBtns([]);
    };
    const handleSaveGenericResponse = (intent,wizardIdProject) => {  
        let project = {
            genericAnswer : GenericResponse,
            intent: intent,
            buttons: GenericBtns,
        };

        if(GenericResponse === "") {
            swalWithBootstrapButtons.fire({
                title: 'Generic Response is empty !',
                confirmButtonText: 'Okay',
            });
        } else if (intentState === 'save' && GenericResponse !== '') {
            setSaveUpdateLoading(true);

            props.addGenericResponse(project,wizardIdProject)
            .then(() => {
                setIntentState('update');
                setSaveUpdateLoading(false);
            })
            .catch(() => {
                setSaveUpdateLoading(false);
                swalWithBootstrapButtons.fire({
                    title: 'Error while saving the generic response. Try Again !',
                    confirmButtonText: 'Okay',
                });
            });

        } else if (intentState === 'update' && GenericResponse !== '') {
            setSaveUpdateLoading(true);

            props.updateGenericResponse(project,wizardIdProject)
            .then(() => {
                setSaveUpdateLoading(false);
            })
            .catch(() => {
                setSaveUpdateLoading(false);
                swalWithBootstrapButtons.fire({
                    title: 'Error while updating the generic response. Try Again !',
                    confirmButtonText: 'Okay',
                });
            });
        }
        
    };

    // Specific:
    const [SpecificResponse, SetSpecificResponse] = useState('');
    const [SpecificKeyBoard, setSpecificKeyBoard] = useState(false);
    const [SpecificEmoji, setSpecificEmoji] = useState(false);
    const [SpecificTags, setSpecificTags] = useState(false);
    const [SpecificBtnType, setSpecificType] = useState('url');
    const [SpecificBtnTitle, setSpecificTitle] = useState('');
    const [SpecificBtnValue, setSpecificValue] = useState('');
    const [SpecificBtns, setSpecificBtns] = useState([]);
    const [SpecificPopover, setSpecificPopover] = useState(false);
    const [SpecificTarget, setSpecificTarget] = useState(null);
    const SpecificRef = useRef(null);
    const resetSpecific = () => {
        SetSpecificResponse('');
        setSpecificTags(false);
        setSpecificEmoji(false);
        setSpecificKeyBoard(false);
        setSpecificType('url');
        setSpecificTitle('');
        setSpecificValue('');
        setSpecificBtns([]);
    };
    const handleSpecificPopover = (event) => {
        setSpecificPopover(!SpecificPopover);
        setSpecificTarget(event.target);
    };

    // Specific Update:
    const [SpecificUpdateResponse, SetSpecificUpdateResponse] = useState('');
    const [SpecificUpdateKeyBoard, setSpecificUpdateKeyBoard] = useState(false);
    const [SpecificUpdateEmoji, setSpecificUpdateEmoji] = useState(false);
    const [SpecificUpdateTags, setSpecificUpdateTags] = useState(false);
    const [SpecificUpdateBtnType, setSpecificUpdateType] = useState('url');
    const [SpecificUpdateBtnTitle, setSpecificUpdateTitle] = useState('');
    const [SpecificUpdateBtnValue, setSpecificUpdateValue] = useState('');
    const [SpecificUpdateBtns, setSpecificUpdateBtns] = useState([]);
    const [SpecificUpdatePopover, setSpecificUpdatePopover] = useState(false);
    const [SpecificUpdateTarget, setSpecificUpdateTarget] = useState(null);
    const SpecificUpdateRef = useRef(null);
    const resetUpdateSpecific = () => {
        setSpecificUpdatePopover(false);
        setSpecificUpdateTarget(null);
        SetSpecificUpdateResponse('');
        setSpecificUpdateKeyBoard(false);
        setSpecificUpdateEmoji(false);
        setSpecificUpdateTags(false);
        setSpecificUpdateType('url');
        setSpecificUpdateTitle('');
        setSpecificUpdateValue('');
        setSpecificUpdateBtns([]);
    };
    const handleSpecificUpdatePopover = (event,product) => {
        // console.log("Product to update =>", product);
        setProductUpdateName(product.product);
        SetSpecificUpdateResponse(product.preAnswer);
        setSpecificUpdateBtns(product.buttons);
        setSpecificUpdatePopover(!SpecificUpdatePopover);
        setSpecificUpdateTarget(event.target);
    };

    // Products
    const [productName, setProductName] = useState('');
    const [productUpdateName, setProductUpdateName] = useState('');
    const [savedProductToSend, setSavedProductToSend] = useState('');
    const [products, setProducts] = useState([]);

    // Categories
    const [newCategory, setNewCategory] = useState('');
    const [categorySelected, setCategorySelected] = useState('');
    const [allCategories, setAllCategories] = useState([]);
    const [CategorieModal, setCategorieModal] = useState(false);
    const [showModalInput, setShowModalInput] = useState(false);
    const handleCategoriesModal = () => {
        setSpecificPopover(false);
        setCategorieModal(!CategorieModal);
    };
    // other:
    const [saveUpdateLoading,setSaveUpdateLoading] = useState(false);
    const [intentState, setIntentState] = useState('save');

    // Categories & Products functions :
    const addNewProduct = (idProjet) => {
        
        // First update api : entity === categorie :
        let productToSend = {
            idPage: props.wizardSelectedPage.id,
            typePage : props.wizardSelectedPage.platform,
            entity: productName,
            intent: props.intent.name,
            genericAnswer: GenericResponse,
            specificAnswer: SpecificResponse,
            categorie: productName,
            buttons: SpecificBtns,
        };

        axios.put(`${host}/api/v1/secure/project/firstupdate/${idProjet}`, productToSend, {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then( res => {
            console.log('first update response =>', res.data.data);

            if (res.data.data === "not exist") {
                // Call the categories api :
                axios.get(`${host}/api/v1/secure/project/get/entitiesproject/${props.wizardSelectedPage.platform}/${props.wizardSelectedPage.id}`)
                .then((res) => {
                    console.log("get categories for modal /get/entitiesproject =>", res.data.data);
                    
                    setAllCategories(res.data.data);
                    setSpecificPopover(!SpecificPopover);
                    setSavedProductToSend(productToSend);
                    setCategorieModal(true);
                    
                    // ==> Update the products array from addNewProductViaModal() <==
                })
                .catch((err) => console.log("addNewProduct WizardIntentDetails =>", err))
            } else if(res.data.data === "exist") {
                swalWithBootstrapButtons.fire({
                    title: 'Product already exists !',
                    confirmButtonText: 'Okay',
                });
            } else {
                // Update the products array :
                let newSelectedIntents = [...selectedIntents];

                newSelectedIntents.map((intent,index) => {
                    if(index === props.indexOfIntent)  {
                        // intent.products = res.data.data.intents[indexOfIntent].answer
                        setProducts([]);
                        setProducts(res.data.data.intents[indexOfIntent].answer);
                    };
                });
                setIntents(newSelectedIntents);
                
                // Reset
                setSpecificPopover(false);
                setProductName('');
                SetSpecificResponse('');

            };
        })
        .catch( err => console.log("Wizard Intent Detail addNewProduct fct first Update API err ===>", err))
    };

    const addNewProductViaModal = (idProjet) => {
        // Send the update with the new category name :) :
        let productToSend = savedProductToSend;
        productToSend.categorie = categorySelected;
    
        // console.log("sendinnng product =>",productToSend);
        // console.log("Category Selected To sennnnd", categorySelected," and product =>>>", productToSend);
        
        return axios.put(host + `/api/v1/secure/project/update/${idProjet}`,productToSend, {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('UPDATE API response =>', res.data);
       
            // set the products from the update api :
            let newSelectedIntents = [...selectedIntents];

            newSelectedIntents.map((_,index) => {
                if(index === props.indexOfIntent)  {
                    //intent.products = res.data.data.intents[indexOfIntent].answer
                    setProducts([]);
                    setProducts(res.data.data.intents[indexOfIntent].answer);
                };
            });
            setIntents(newSelectedIntents);
            
            // Reset
            setProductName('');
            SetSpecificResponse('');
            setCategorySelected('');
            setCategorieModal(false);
        })
        .catch((err) => console.log("addNewProductViaModal => api update err =>> ", err))
    };

    const updateProduct = (idProjet) => {
        // swalWithBootstrapButtons.fire({
        //     title: 'En cours de dev..',
        //     confirmButtonText: 'Okay',
        // });

        let productToSend = {
            idPage: props.wizardSelectedPage.id,
            typePage : props.wizardSelectedPage.platform,
            entity: productUpdateName,
            intent: props.intent.name,
            genericAnswer: GenericResponse,
            specificAnswer: SpecificUpdateResponse,
            buttons: SpecificUpdateBtns,
            // categorie: productUpdateName,
        };

        axios.put(host + `/api/v1/secure/project/entitiesupdate/${props.wizardIdProject}`,productToSend,
        {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('updateProduct API response =>', res.data);
            // newProducts.splice(index,1);
            // setProducts(newProducts);   
            // setDeleteProductSpinner(false);
        })
        .catch((err) => {
            console.log("updateProduct API error =>> ", err)
            swalWithBootstrapButtons.fire({
                title: 'Error while updating the product. Try Again!',
                confirmButtonText: 'Okay',
            });
        });

    };

    const deleteProduct = (product,index) => {
        
        let newProducts = [...products];
        
        let objectToSend = {
            intent: intent.name,
            answer: product._id,
        };
        
        console.log("To delete That", objectToSend);

        axios.put(host + `/api/v1/secure/project/${props.wizardIdProject}/remove/entity`,objectToSend,{
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('deleteProduct API response =>', res.data);
            newProducts.splice(index,1);
            setProducts(newProducts);   
        })
        .catch((err) => {
            console.log("deleteProduct api err =>> ", err)
            swalWithBootstrapButtons.fire({
                title: 'Error while deleting the product. Try Again!',
                confirmButtonText: 'Okay',
            });
        });
 
    };

    const pushNewCategory = () => {  
        
        let categoryToPush = {
            name : newCategory
        };

        setAllCategories(categories => [...categories, categoryToPush]);
        setCategorySelected(categoryToPush.name);
        setShowModalInput(false); 
        setNewCategory('');
    };

    const deleteIntent = (intent,index) => {
        // console.log("U want to delete this intent =>", intent);
        // intent: nom intent
        let newIntents = [...selectedIntents];
    
        let objectToSend = {
            intent: intent.name,
        };

        console.log("i'm gonna delete intent ya nour with =>", objectToSend);

        axios.put(host + `/api/v1/secure/project/intents/remove/${props.wizardIdProject}`, objectToSend, {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem('authtoken')
            }
        })
        .then(res => {
            console.log('deleteIntent API response =>', res.data);
            newIntents.splice(index,1);
            setIntents(newIntents);   
        })
        .catch((err) => {
            console.log("deleteIntent api err =>> ", err)
            swalWithBootstrapButtons.fire({
                title: 'Error while deleting the intent. Try Again!',
                confirmButtonText: 'Okay',
            });
        });

        // setIntents(selectedIntents.filter( intent =>( intent !== props.intent.name )))
    };
     
    useEffect(() => {
        if(isExistingProject && intentStatus === "old") {
            console.log("Existing Proj", isExistingProject);
            setGenericResponse(isExistingProject[indexOfIntent].genericAnswer);
            setGenericBtns(isExistingProject[indexOfIntent].buttons)
            setProducts(isExistingProject[indexOfIntent].answer);
            setIntentState('update');
        }
    },[isExistingProject]);

    return (
        <Col lg={5} className="mb-3 mr-3" key={indexOfIntent}>
            
            <div className="d-flex flex-column wizard-config-intents-container" style={{borderRadius: '0.5rem'}}>
                
                {/* Header */}
                <Row>
                    <Col lg={12}>
                        <Row className="justify-content-between mx-0 p-0 wizard-config-intents-top-row">
                            <div className="d-flex">
                                <p className="my-auto wizard-config-intent-title p-2">{intent.name}</p> 
                            </div>

                            <div className="d-flex mr-1">
                                <FaTrashAlt  color={"#818E94"} size={18} className="m-auto" style={{cursor: 'pointer'}} onClick={ () => deleteIntent(intent,indexOfIntent)} />
                                <IoIosArrowDown color={"#818E94"} size={18} className="my-auto ml-1" style={{cursor: 'pointer'}} />
                            </div>
                        </Row>
                    </Col>
                </Row>

                <div className="mb-2">
                  <WizardMiracleInput  
                    label={"Generic Response"}
                    response={GenericResponse} handleResponse={setGenericResponse}
                    showTags={GenericTags} handleTags={setGenericTags}
                    showEmojis={GenericEmojis} handleEmojis={setGenericEmojis}
                    showKeyboard={GenericKeyboard} handleKeyboard={setGenericKeyboard}
                    BtnType={GenericBtnType} handleBtnType={setGenericBtnType}
                    BtnTitle={GenericBtnTitle} handleBtnTitle={setGenericBtnTitle}
                    BtnValue={GenericBtnValue} handleBtnValue={setGenericBtnValue}
                    Btns={GenericBtns} handleBtns={setGenericBtns}
                    reset={resetGeneric}
                  />
                </div>

                <div>
                    {/* Save Update Button */}
                    <div className="d-flex">
                        {
                            intentState === 'save' 
                            ?
                            (
                                !saveUpdateLoading 
                                ?
                                <div onClick={() => handleSaveGenericResponse(props.intent.name,props.wizardIdProject)} className="ml-auto mr-2 mb-2 py-2 px-3 wizard-config-update-btn">
                                    Save
                                </div>
                                :
                                <div className="d-flex ml-auto mr-2 mb-2 py-2 px-4 wizard-config-update-btn">
                                    <Spinner animation="grow" size="sm" variant="light" className="m-auto" />
                                </div>
                            )
                            :
                            (
                                !saveUpdateLoading 
                                ?
                                <div onClick={() => handleSaveGenericResponse(props.intent.name,props.wizardIdProject)} className="ml-auto mr-2 mb-2 py-2 px-3 wizard-config-update-btn">
                                    Update
                                </div>
                                :
                                <div className="d-flex ml-auto mr-2 mb-2 py-2 px-4 wizard-config-update-btn">
                                    <Spinner animation="grow" size="sm" variant="light" className="m-auto" />
                                </div>
                            )
                        }
                    </div>

                    {/* PRODUCTS */}
                    {intentState === 'update' &&
                    <Col lg={11} className="mx-auto mt-1">
                        <Row className="justify-content-between">
                            {products.length > 0 && products.map((product,index) => 
                                product.product.length > 0 
                                &&
                                <Col key={index} lg={5}  className={product.default ? "d-flex flex-row mb-3 py-2 wizard-config-product-box-exist" : "d-flex flex-row mb-3 py-2 wizard-config-product-box"}>
                                    {
                                        product.product.length > 8
                                        ?
                                        <p onClick={(e) => handleSpecificUpdatePopover(e,product)} data-for='entityTip' data-tip={product.product} className="m-auto wizard-config-product-name">
                                            {product.product.substring(0,7)}...
                                        </p>
                                        :
                                        <p onClick={(e) => handleSpecificUpdatePopover(e,product)} className={product.default ? "m-auto wizard-config-product-name-exist" : "m-auto wizard-config-product-name"}>
                                            {product.product}
                                        </p>
                                    }
                                    <AiOutlineCloseCircle className="my-auto ml-1" color={"#F49BCC"} size={"20"} style={{cursor: 'pointer'}} onClick={() => deleteProduct(product,index)} />
                                    <ReactTooltip id='entityTip' textColor='#fff' backgroundColor='#E5007D' />
                                </Col> 
                                )
                            } 
    
                            <Col lg={5} onClick={handleSpecificPopover} className="wizard-config-add-product-box mb-3 py-2 px-3" style={{cursor: 'pointer'}}>
                                <Row className="d-flex justify-content-around">
                                    <MdAddCircleOutline className="my-auto mr-2" color={"#4080FC"} size={20} />
                                    <p className="wizard-config-add-product-name my-auto">Add product</p>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    }
                </div>
            </div>
             
            {/* Specific Response Popover */}
            <div ref={SpecificRef}>
                <Overlay
                    show={SpecificPopover}
                    target={SpecificTarget}
                    container={SpecificRef.current}
                    placement="top"
                >
                    <Popover id="popover-specific"> 
                        <Popover.Content>
                            <div className="d-flex">
                                <IoIosClose className="ml-auto" color={'#9F9F9F'} size={'25'} onClick={handleSpecificPopover} style={{cursor: 'pointer'}} />
                            </div>

                            <div className="m-2">
                                <label className="wizard-config-textarea-label">Product name</label>
                                <input onChange={(e) => setProductName(e.target.value)} value={productName} className="wizard-config-product-input" />
                            </div>

                            <div className="mb-2">
                                <WizardMiracleInput  
                                    label={"Specific Response"}
                                    response={SpecificResponse} handleResponse={SetSpecificResponse}
                                    showTags={SpecificTags} handleTags={setSpecificTags}
                                    showEmojis={SpecificEmoji} handleEmojis={setSpecificEmoji}
                                    showKeyboard={SpecificKeyBoard} handleKeyboard={setSpecificKeyBoard}
                                    BtnType={SpecificBtnType} handleBtnType={setSpecificType}
                                    BtnTitle={SpecificBtnTitle} handleBtnTitle={setSpecificTitle}
                                    BtnValue={SpecificBtnValue} handleBtnValue={setSpecificValue}
                                    Btns={SpecificBtns} handleBtns={setSpecificBtns}
                                    reset={resetSpecific}
                                />
                            </div>

                            <div className="mr-2 ml-2 mt-2 mb-2 d-flex">
                                <div className="ml-auto mb-2 py-3 px-3 wizard-config-update-btn" onClick={() => addNewProduct(props.wizardIdProject)}>Add</div>
                            </div>
                        </Popover.Content>
                    </Popover>
                </Overlay>
            </div>
            
            {/* Specific Response Update Popover */}
            <div ref={SpecificUpdateRef}>
                <Overlay
                    show={SpecificUpdatePopover}
                    target={SpecificUpdateTarget}
                    container={SpecificUpdateRef.current}
                    placement="top"
                >
                    <Popover id="popover-specific"> 
                        <Popover.Content>
                            <div className="d-flex">
                                <IoIosClose className="ml-auto" color={'#9F9F9F'} size={'25'} onClick={resetUpdateSpecific} style={{cursor: 'pointer'}} />
                            </div>

                            <div className="m-2">
                                <label className="wizard-config-textarea-label">Product name</label>
                                <input onChange={(e) => setProductUpdateName(e.target.value)} value={productUpdateName} disabled={true} style={{cursor: 'not-allowed'}} className="wizard-config-product-input" />
                            </div>

                            <div className="mb-2">
                                <WizardMiracleInput  
                                    label={"Update Specific Response"}
                                    response={SpecificUpdateResponse} handleResponse={SetSpecificUpdateResponse}
                                    showTags={SpecificUpdateKeyBoard} handleTags={setSpecificUpdateKeyBoard}
                                    showEmojis={SpecificUpdateEmoji} handleEmojis={setSpecificUpdateEmoji}
                                    showKeyboard={SpecificUpdateTags} handleKeyboard={setSpecificUpdateTags}
                                    BtnType={SpecificUpdateBtnType} handleBtnType={setSpecificUpdateType}
                                    BtnTitle={SpecificUpdateBtnTitle} handleBtnTitle={setSpecificUpdateTitle}
                                    BtnValue={SpecificUpdateBtnValue} handleBtnValue={setSpecificUpdateValue}
                                    Btns={SpecificUpdateBtns} handleBtns={setSpecificUpdateBtns}
                                    reset={resetUpdateSpecific}
                                />
                            </div>

                            <div className="mr-2 ml-2 mt-2 mb-2 d-flex">
                                <div className="ml-auto mb-2 py-3 px-3 wizard-config-update-btn" onClick={() => updateProduct(props.wizardIdProject)}>Update</div>
                            </div>
                        </Popover.Content>
                    </Popover>
                </Overlay>
            </div>

            {/* Categories Modal */}
            <Modal show={CategorieModal} onHide={handleCategoriesModal} centered>
                <Modal.Body>          
                    <Row className="d-flex mb-3">
                        <IoIosClose className="ml-auto mr-3" color={'#9F9F9F'} size={'28'} onClick={handleCategoriesModal} style={{cursor: 'pointer'}} />
                    </Row>

                    <Row className="d-flex mb-3">
                        <p className="m-auto wizard-config-intent-title">Please specify the category</p>
                    </Row>

                    <Row className="mb-3">
                        <Col lg={1} />

                        <Col lg={10} className="d-flex justify-content-between p-0 mx-0">
                            <Col lg={8} className="d-flex">
                                {showModalInput && 
                                    <input 
                                        type="text" 
                                        className="mr-auto w-100 my-auto add-new-category-input-container" 
                                        onChange={(e) => setNewCategory(e.target.value)}
                                        value={newCategory}
                                        // onKeyDown={(e) => e.key === 'Enter' && pushNewCategory()}
                                    />
                                }
                            </Col>

                            <Col lg={4} className="d-flex">
                                {!showModalInput
                                &&
                                <p className="ml-auto my-auto wizard-config-intent-name" style={{cursor: 'pointer'}} onClick={() => setShowModalInput(true)}>
                                    create new
                                </p>
                                }
                            </Col>
                        </Col>

                        <Col lg={1} />
                    </Row>

                    <Row>
                        <Col lg={1} />

                        <Col lg={10}>
                            <Form.Group className="d-flex w-100">
                                <Form.Control 
                                    className="wizard-config-intent-title" 
                                    as="select" 
                                    disabled={showModalInput || allCategories.length === 0} 
                                    onChange={(e) => setCategorySelected(e.target.value)}
                                    value={categorySelected} 
                                    custom
                                >
                                    <option className="wizard-config-intent-title">Choose Category...</option>
                                    {allCategories.length > 0 
                                    && 
                                    allCategories.map((category,index) => 
                                        <option key={index} className="wizard-config-intent-title" value={category.name}>{category.name}</option>
                                    )}
                                </Form.Control>
                            </Form.Group>
                        </Col>

                        <Col lg={1} /> 
                    </Row>
                    
                    <Row className="mb-3">
                        {showModalInput
                        ?
                        <div 
                            className="m-auto mb-2 py-3 px-4 wizard-config-update-btn" 
                            style={{backgroundColor: '#009EE3', cursor:'pointer' }} 
                            onClick={() => pushNewCategory()}
                        >
                        Create
                        </div>
                        :
                        <div 
                        className="m-auto mb-2 py-3 px-4 wizard-config-update-btn" 
                        style={{backgroundColor: allCategories.length > 0 && categorySelected ? '#009EE3' : '#707070', cursor: allCategories.length > 0 && categorySelected ? 'pointer' : 'not-allowed', }} 
                        onClick={() => allCategories.length > 0 && categorySelected ? addNewProductViaModal(props.wizardIdProject) : null}
                        >
                        Add
                        </div>
                        }
                        {/* <div 
                            className="m-auto mb-2 py-3 px-4 wizard-config-update-btn" 
                            style={{backgroundColor: allCategories.length > 0 && categorySelected ? '#009EE3' : '#707070', cursor: allCategories.length > 0 && categorySelected ? 'pointer' : 'not-allowed', }} 
                            onClick={() => allCategories.length > 0 && categorySelected ? addNewProductViaModal(props.wizardIdProject) : null}
                        >
                        Add
                        </div> */}
                    </Row>
                </Modal.Body>
            </Modal>
            
        </Col>
    )
}

const mapStateToProps = (state) => ({
    wizardIdProject: state.wizardR.wizardIdProject,
    wizardIntentType: state.wizardR.wizardIntentType,
    wizardSelectedPage: state.wizardR.wizardSelectedPage,
    wizardSelectedPost: state.wizardR.wizardSelectedPost,
    // wizardStep: state.wizardR.wizardStep,
})

export default connect(mapStateToProps, { addGenericResponse, updateGenericResponse })(WizardIntentDetails)


/**
 *
 * 
 * 
 *  {
        products.length > 0 &&
        products[props.indexOfIntent].products.map((product,index) => 
            <Col key={index} lg={5} className={product.default ? "d-flex flex-row mb-3 py-2 wizard-config-product-box-exist" : "d-flex flex-row mb-3 py-2 wizard-config-product-box"}>
                {
                    product.product.length > 8
                    ?
                    <p data-for='entityTip' data-tip={product.product} className="m-auto wizard-config-product-name">{product.product.substring(0,7)}...</p>
                    :
                    <p className={product.default ? "m-auto wizard-config-product-name-exist" : "m-auto wizard-config-product-name"}>
                        {product.product}
                    </p>
                }
                <AiOutlineCloseCircle className="my-auto ml-1" color={"#F49BCC"} size={"20"} style={{cursor: 'pointer'}} onClick={() => deleteProduct(product,index)} />
                <ReactTooltip id='entityTip' textColor='#fff' backgroundColor='#E5007D' />
            </Col> 
        )
    }  
 * 
 * {
    products.length > 0 &&
    products.map((product,index) => 

        <Col key={index} lg={5} className="d-flex flex-row mb-3 py-2 wizard-config-product-box">
            {
                product.entity.length > 8
                ?
                <p data-for='entityTip' data-tip={product.entity} className="m-auto wizard-config-product-name">{product.entity.substring(0,7)}...</p>
                :
                <p className="m-auto wizard-config-product-name">{product.entity}</p>
            }
            <AiOutlineCloseCircle className="my-auto ml-1" color={"#F49BCC"} size={"20"} style={{cursor: 'pointer'}} onClick={() => deleteProduct(product,index)} />
            <ReactTooltip id='entityTip' textColor='#fff' backgroundColor='#E5007D' />
        </Col> 
    )
}   
* 
 * 
 * 
 * Generic Input
 <div className="mr-2 ml-2 mt-2 mb-2">
 <label className="wizard-config-textarea-label">Generic Response</label>
 <Col lg={12}>
     <Row className="d-flex mb-1">
         <div className="d-flex mr-2">
             <Image src={showGenericTags ? BALISE_ICON_ACTIVE : BALISE_ICON} onClick={handleGenericTagsPopover} className="m-auto my-agents-msgs-tiny-icon" style={{ width: '19px', cursor: 'pointer'}} />    
         </div>                                

         <div className="d-flex mr-2">
             <Image src={showEmojiPickerGeneric ? EMO_ICON_ACTIVE : EMO_ICON} onClick={handleGenericEmojiPopover} className="m-auto my-agents-msgs-tiny-icon" style={{ width: '18px', cursor: 'pointer'}} />
         </div>

         <div className="d-flex mr-2">
             <Image src={showKeyBoardGeneric ? LETTER_ICON_ACTIVE : LETTER_ICON} onClick={handleGenericKeyboardPopover} className="m-auto my-agents-msgs-tiny-icon" style={{ width: '21px', cursor: 'pointer'}} />
             
             {showKeyBoardGeneric &&
                 <div className="wizard-config-keyboard-container">
                     <Keyboard onChange={(input) => handleArabicKeyboardChange(input,"generic")} onKeyPress={(button) => onKeyPress(button,"generic")} layout={layout} />
                 </div>
             }
             {showEmojiPickerGeneric &&
                 <div className="wizard-config-emoji-container" >
                     <Picker onEmojiClick={(e,obj) => onEmojiClick(e,obj,"generic")} />
                 </div>
             }
         </div>
     </Row>
 </Col>
 <textarea onChange={(e) => setGenericResponse(e.target.value)} onFocus={() => console.log("Hey I'm Focused")} value={genericResponse} className="wizard-config-textarea" rows="5"/>
</div>



 Generic Tags modal 
<Modal show={showGenericTags} size="sm" centered>
<Row className="d-flex w-100">
    <IoIosClose className="ml-auto my-auto pt-1" color={'#9F9F9F'} size={30} onClick={() => setShowGenericTags(false)} style={{cursor: 'pointer'}} />
</Row>

<Row className="d-flex w-100">
    <h3 className="m-auto wizard-config-intent-title">Choose one tag variable</h3>
</Row>

<Modal.Body>  
    <Row className="d-flex">
        <Col xs={12} className="m-auto d-flex">
            <p className="m-auto wizard-config-tag" onClick={() => pushSelectedTag('##FIRST_NAME##',"generic")}>First Name</p>
        </Col>
        <Col xs={12} className="m-auto d-flex">
            <p className="m-auto wizard-config-tag" onClick={() => pushSelectedTag('##LAST_NAME##',"generic")}>Last Name</p>
        </Col>
        <Col xs={12} className="m-auto d-flex">
            <p className="m-auto wizard-config-tag" onClick={() => pushSelectedTag('##FULL_NAME##',"generic")}>Full Name</p>
        </Col>
        <Col xs={12} className="m-auto d-flex">
            <p className="m-auto wizard-config-tag" onClick={() => pushSelectedTag('##PAGE_NAME##',"generic")}>Page Name</p>
        </Col>
    </Row>
</Modal.Body>
</Modal>

  FUNCTIONS 
    // Generic :
    const handleGenericTagsPopover = (event) => {
        setShowEmojiPickerGeneric(false);
        setShowKeyBoardGeneric(false);
        setShowGenericTags(!showGenericTags);
        setTargetGenericTags(event.target);

        //Specific
        setShowSpecificTags(false);
        setShowEmojiPicker(false);
        setShowKeyBoard(false);
    };

    const handleGenericKeyboardPopover = () => {
        setShowEmojiPickerGeneric(false);
        setShowGenericTags(false);
        setShowKeyBoardGeneric(!showKeyBoardGeneric);

        //Specific
        setShowSpecificTags(false);
        setShowEmojiPicker(false);
        setShowKeyBoard(false);
    };

    const handleGenericEmojiPopover = () => {
        setShowGenericTags(false);
        setShowKeyBoardGeneric(false);
        setShowEmojiPickerGeneric(!showEmojiPickerGeneric);

        //Specific
        setShowSpecificTags(false);
        setShowEmojiPicker(false);
        setShowKeyBoard(false);
    };



     Specific Tags Popover 
    <div ref={refSepecificTags}>
    <Overlay
        show={showSpecificTags}
        target={targetSpecificTags}
        placement="top"
        container={refSepecificTags.current}
        containerPadding={20}
    >
        <Row className="d-flex" style={{ width: '200px', height: '200px', background: '#fff', borderRadius: '0.3rem', marginBottom: "9px"}}>
            <Col xs={12} className="m-auto d-flex">
                <p className="m-auto wizard-config-tag">First Name</p>
            </Col>
            <Col xs={12} className="m-auto d-flex">
                <p className="m-auto wizard-config-tag">Last Name</p>
            </Col>
            <Col xs={12} className="m-auto d-flex">
                <p className="m-auto wizard-config-tag">Full Name</p>
            </Col>
            <Col xs={12} className="m-auto d-flex">
                <p className="m-auto wizard-config-tag">Page Name</p>
            </Col>
        </Row>
    </Overlay>
</div>


 const handleSpecificKeyboardPopover = () => {
        // Generic
        setShowEmojiPickerGeneric(false);
        setShowKeyBoardGeneric(false);
        setShowGenericTags(false);
        
        //Specific
        setShowSpecificTags(false);
        setShowEmojiPicker(false);
        setShowKeyBoard(!showKeyBoard);
    };

    const handleSpecificEmojiPopover = () => {
        // Generic
        setShowEmojiPickerGeneric(false);
        setShowKeyBoardGeneric(false);
        setShowGenericTags(false);
        
        //Specific
        setShowSpecificTags(false);
        setShowKeyBoard(false);
        setShowEmojiPicker(!showEmojiPicker);
    };

    const handleSpecificTagsPopover = (event) => {
        // Generic
        setShowEmojiPickerGeneric(false);
        setShowKeyBoardGeneric(false);
        setShowGenericTags(false);

        // Specific
        setShowEmojiPicker(false);
        setShowKeyBoard(false);
        SpecificPopover(false);
        setShowSpecificTags(true);

    };

     const handleArabicKeyboardChange = (input,type) => {
        if(type === "generic") {
            let newGenericResponse = `${genericResponse}${input.charAt(input.length - 1)}`;
            setGenericResponse(newGenericResponse);
        } else {
            let newSpecifiResponse = `${SpecificResponse}${input.charAt(input.length - 1)}`;
            SetSpecificResponse(newSpecifiResponse);
        }
    };
    
    const onKeyPress = (button,type) => {
        if(type === "generic") {
            if (button.toString() == "{enter}") {
                setShowKeyBoardGeneric(false);
            }
        } else {
            if (button.toString() == "{enter}") {
                setShowKeyBoard(false);
            }
        }
    };
    
    const onEmojiClick = (event, emojiObject,type) => {
        if(type === "generic") {
            let newGenericResponse = `${genericResponse}${emojiObject.emoji}`;
            setGenericResponse(newGenericResponse);
        } else {
            let newSpecifiResponse = `${SpecificResponse}${emojiObject.emoji}`;
            SetSpecificResponse(newSpecifiResponse);
        }
    };

    const pushSelectedTag = (tag,type) => {

        if(type === "generic") {
            let newGenericResponse = `${genericResponse} ${tag} `;
            setGenericResponse(newGenericResponse);
            setShowGenericTags(false);
        } else {
            let newSpecifiResponse = `${SpecificResponse} ${tag} `;
            SetSpecificResponse(newSpecifiResponse);
            setShowSpecificTags(false);
        }
    };


    {/* Specific Tags modal 
    <Modal show={showSpecificTags} size="sm" centered>   
    <Row className="d-flex w-100">
        <IoIosClose className="ml-auto my-auto pt-1" color={'#9F9F9F'} size={30} onClick={() => { setShowSpecificTags(false); SpecificPopover(true); }} style={{cursor: 'pointer'}} />
    </Row>

    <Row className="d-flex w-100">
        <h3 className="m-auto wizard-config-intent-title">Choose one tag variable</h3>
    </Row>
   
    <Modal.Body>  
        <Row className="d-flex">
            <Col xs={12} className="m-auto d-flex">
                <p className="m-auto wizard-config-tag" onClick={() => { pushSelectedTag('##FIRST_NAME##', "specific"); setShow(true); }}>First Name</p>
            </Col>
            <Col xs={12} className="m-auto d-flex">
                <p className="m-auto wizard-config-tag" onClick={() => { pushSelectedTag('##LAST_NAME##', "specific"); setShow(true); }}>Last Name</p>
            </Col>
            <Col xs={12} className="m-auto d-flex">
                <p className="m-auto wizard-config-tag" onClick={() => { pushSelectedTag('##FULL_NAME##', "specific"); setShow(true); }}>Full Name</p>
            </Col>
            <Col xs={12} className="m-auto d-flex">
                <p className="m-auto wizard-config-tag" onClick={() => { pushSelectedTag('##PAGE_NAME##', "specific"); setShow(true); }}>Page Name</p>
            </Col>
        </Row>
    </Modal.Body>
</Modal>

{/* Specific Btns (Quicks replies Modal) 
<Modal show={showButtonsModal} size="sm" centered>   
    <Modal.Body>  
        <Row className="justify-content-center">
            <Col md={12}>
                <Row>
                    <Col md={12} className="mt-2 mb-3">
                        <Form.Control as="select" onChange={(e) => setQuickReplyType(e.target.value)} defaultValue="url">
                            <option value="url">Url</option>
                            <option value="call">Call</option>
                        </Form.Control>
                    </Col>
                    
                    <Col md={12} className="mt-2">
                        <Row className="d-flex">
                            <Col md={12}>
                                <Form.Control type={"text"} onChange={(e) => setQuickReplyTitle(e.target.value)} value={quickReplyTitle}  placeholder="Title" />
                            </Col>

                            <Col md={12} className="d-flex">
                                <p className="ml-auto wizard-config-input-feedback">Remaining characters: 20</p>
                            </Col>
                        </Row>
                    </Col>

                    <Col md={12} className="mb-2">
                        <Form.Control type={quickReplyType === "url" ? "text" : "number"} onChange={(e) => setQuickReplyValue(e.target.value)} value={quickReplyValue}  placeholder={quickReplyType === "url" ? "www.example.com" : "(+216) 99 999 999"}  />
                    </Col>
                </Row>
            </Col>

            <Col md={11} className="d-flex pl-1">
                <div className="wizard-pages-active-btn py-3 px-5" onClick={() => addNewQuickReply()}>
                    OK
                </div>
            </Col>
           
        </Row>
    </Modal.Body>
</Modal>



 <div className="m-2">
    <label className="wizard-config-textarea-label">Specific response</label>
    <Row>
            <Col lg={12}>
            <Row className="d-flex mb-1 ml-2">
                <div className="d-flex mr-2">
                    <Image src={showSpecificTags ? BALISE_ICON_ACTIVE : BALISE_ICON} onClick={handleSpecificTagsPopover} className="m-auto my-agents-msgs-tiny-icon" style={{ width: '19px', cursor: 'pointer'}} />    
                </div>                                

                <div className="d-flex mr-2">
                    <Image src={showEmojiPicker ? EMO_ICON_ACTIVE : EMO_ICON} onClick={handleSpecificEmojiPopover} className="m-auto my-agents-msgs-tiny-icon" style={{ width: '18px', cursor: 'pointer'}} />
                </div>

                <div className="d-flex mr-2">
                    <Image src={showKeyBoard ? LETTER_ICON_ACTIVE : LETTER_ICON} onClick={handleSpecificKeyboardPopover} className="m-auto my-agents-msgs-tiny-icon" style={{ width: '21px', cursor: 'pointer'}} />
                    
                    {showKeyBoard &&
                        <div className="wizard-config-keyboard-container">
                            <Keyboard onChange={handleArabicKeyboardChange} onKeyPress={onKeyPress} layout={layout} />
                        </div>
                    }
                    {showEmojiPicker &&
                        <div className="wizard-config-emoji-container" >
                            <Picker onEmojiClick={onEmojiClick} />
                        </div>
                    }
                </div>
            </Row>
        </Col> 

        <Col lg={12} className="mb-2">
        <textarea onChange={(e) => setSpecificResponse(e.target.value)} value={SpecificResponse} className="wizard-config-textarea" rows="5"/>
        <hr style={{borderColor: '#B4B4B4', borderWidth: '1.5px'}}/>
    </Col> 
    
        Quick replies 
        <Col lg={12} className="d-flex flex-row mb-2">
    
        {quickReplies.map((quickReply,index) => 
            <Col key={index} lg={3} className="mr-2">
                <Row className="wizard-config-quick-replies-btn py-1">
                    <p className="m-auto" data-for='quickReplyTip' data-tip={quickReply.value}>{quickReply.type}</p>
                    <AiOutlineCloseCircle className="m-auto" color={"white"} size={"17"} style={{cursor: 'pointer'}} onClick={() => deleteQuickReply(index)} />
                </Row>
                <ReactTooltip id='quickReplyTip' textColor='#fff' backgroundColor='#009EE3' />
            </Col>
        )}

        // ADD BUTTON
        {quickReplies.length < 3
        &&
        <Col lg={3}>
            <Row 
                className="wizard-config-quick-replies-btn py-1" 
                
                onClick={() =>{ setShowButtonsModal(true); setShow(false); }}
            >
                <p className="m-auto">Add button</p>
            </Row>
        </Col>
        }

    </Col>
</Row>
</div>

 */