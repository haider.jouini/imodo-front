import React from 'react'

import {
    Row,
    Col,
} from 'react-bootstrap';

import { connect } from 'react-redux';

import { IoIosCheckmark } from 'react-icons/io';

function WizardSteps(props) {

    const allSteps = [
        { title: 'Page Selection', number: 1, circleClass: 'wizard-step-circle-active', numClass: 'wizard-step-num-active', titleClass: 'wizard-step-title-active'},
        { title: 'Post Selection', number: 2, circleClass: 'wizard-step-circle', numClass: 'wizard-step-num', titleClass: 'wizard-step-title'},
        { title: 'Automation config', number: 3, circleClass: 'wizard-step-circle', numClass: 'wizard-step-num', titleClass: 'wizard-step-title'},
        { title: 'Test & validation', number: 4, circleClass: 'wizard-step-circle', numClass: 'wizard-step-num', titleClass: 'wizard-step-title'},
    ];

    return (
    <Row className="d-flex mt-3">
                    
        <Col lg={1} />

        {
            allSteps.map((step,index) => {
                let comp = props.wizardStep === index;

                return (
                    <Col key={index} className="d-flex flex-column">
                        {/* <div className={props.wizardStep == index ? circleClassActive : circleClass}> */}
                        {/* <div className={"d-flex m-auto" + (comp ? 'wizard-step-circle-active' : 'wizard-step-circle')}> */}
                        <div className={`${step.number - 1 < props.wizardStep || comp ? 'wizard-step-circle-active' : 'wizard-step-circle'} d-flex m-auto`}>
                            {
                            step.number - 1 < props.wizardStep
                            ?
                            <IoIosCheckmark className="m-auto" color={'white'} size={35} />
                            :
                            <p className={`${step.number - 1 < props.wizardStep || comp ? 'wizard-step-num-active' : 'wizard-step-num'} m-auto`}>{step.number}</p>
                            }
                        </div>
                        <div className="mx-auto mt-2">
                            <p className={`${step.number - 1 < props.wizardStep || comp ? 'wizard-step-title-active' : 'wizard-step-title'}`}>{step.title}</p>
                        </div>
                    </Col>
                )
            })

        }
        
        <Col lg={2} />
      
    </Row>
    )
}

const mapStateToProps = (state) => {
    return {
        wizardStep: state.wizardR.wizardStep,
    }
};

export default connect(mapStateToProps, {})(WizardSteps);
