import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { nextWizardStep,getExistingProject, getAllIntents, trainAgentPage, trainAgentPost } from '../../../redux/actions/wizardActions';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import ReactTooltip from 'react-tooltip';

import WizardIntentDetails from './WizardIntentDetails';

import {
    Row,
    Col,
    Spinner
} from 'react-bootstrap';

export const WizardConfig = (props) => {

    // Popup Config :
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'wizard-pages-active-btn py-2 px-3',
        },
        buttonsStyling: false
    });

    const [selectedIntents, setSelectedIntents] = useState([]);
    const [btnLoading, setBtnLoading] = useState(false);


    const checkIntent = (intent) => {
        let found = selectedIntents.findIndex((intenta) => intenta.name === intent);

        if(found > -1) {
           return true;
        } else {
            return false;
        }
    };

    const newIntent = (intent) => {
        let found = selectedIntents.findIndex((intenta) => intenta.name === intent);
        
        // if already clicked swal the popup
        if(found > -1) {    
            swalWithBootstrapButtons.fire({ title: `Intent: "${intent}" is already selected`,confirmButtonText: 'Okay' });
        } else {
            // Intent selected and create an array for the products to render it from the backend
            let intentToPush = {
                name: intent,
                products: [],
                status: 'new',
            };

            console.log("Im pushing", intentToPush)

            setSelectedIntents(selectedIntents => [...selectedIntents, intentToPush ]);
        }
    };

    const goToTrainAgent = () => {
        if(selectedIntents.length > 0) {
            setBtnLoading(true);

            if(props.wizardIntentType === "generic") {
                props.trainAgentPage(selectedIntents,props.wizardIdProject)
                .then(() => props.nextWizardStep());
            } else {
                props.trainAgentPost(selectedIntents,props.wizardIdProject)
                .then(() => props.nextWizardStep());
            }
        } else {
            swalWithBootstrapButtons.fire({ title: `You have to add at least one intent !`, confirmButtonText: 'Okay' });
        }
    };

    useEffect(() => {
        props.getExistingProject(props.wizardIdProject);
        props.getAllIntents(props.wizardIntentType);
    },[]);

    useEffect(() => {
        if(props.wizardExistingProjectIntents) {
            // Set selected Intent from the existing project directly :
            let intentToPush = {};

            props.wizardExistingProjectIntents.map(intent => {
                intentToPush = {
                    name: intent.name,
                    products: intent.answer,
                    status: 'old',
                };
                setSelectedIntents(selectedIntents => [...selectedIntents, intentToPush]);
            });
        }
        // To Remove the listner
        return () => {

        };
    },[props.wizardExistingProjectIntents]);


    return (
        <Row className="mt-5">  
            <Col lg={1} />
    
            <Col lg={9} className="wizard-pages-container mb-3" style={{backgroundColor: 'white'}}>
                
                {/* INTENTS */}
                <Row className="d-flex">
                    <Col lg={12} className="d-flex flex-column p-3">
                        <div className="d-flex flex-column wizard-config-intents-container">
                            <p className="mr-auto ml-2 p-2 wizard-config-intent-title">Select Intents</p>
                            
                            <Row className="mb-3 mx-2 my-0 d-flex">
                                {
                                props.wizardIntents 
                                &&
                                props.wizardIntents.map((intent, index) => (
                                    <Col key={index} lg={1} onClick={() => newIntent(intent)} className={checkIntent(intent) ? "d-flex wizard-config-intent-box-active py-1 mb-2 mr-1" : "d-flex wizard-config-intent-box py-1 mb-2 mr-1"}>
                                        {intent.length > 6
                                        ?
                                        <p data-for='intentTip' data-tip={intent} className="m-auto wizard-config-intent-name">{intent.substring(0,5)}..</p>
                                        :
                                        <p className="m-auto wizard-config-intent-name">{intent}</p>
                                        }
                                        <ReactTooltip id='intentTip' textColor='#fff' backgroundColor='#E5007D' />
                                    </Col>
                                ))}
                            </Row>
                        </div>
                    </Col>
                </Row>
                
                <Row className="d-flex mt-1">  
                    {selectedIntents &&
                        selectedIntents.map((intent,index) => 
                        <WizardIntentDetails 
                            indexOfIntent={index} 
                            intent={intent} 
                            wizardIdProject={props.wizardIdProject} 
                            setIntents={setSelectedIntents} 
                            selectedIntents={selectedIntents} 
                            isExistingProject={props.wizardExistingProjectIntents ? props.wizardExistingProjectIntents : false} 
                            intentStatus={intent.status}
                        />
                    )}
                </Row>
             
                {/* BTNS */}
                <Col lg={12}>
                    <Row className="mb-3">
                        <Col lg={6}>
                            {/* <Row className="d-flex">
                                <div className="wizard-pages-inactive-btn d-flex py-2 px-4 ml-3 mr-auto my-auto" onClick={() => props.prevWizardStep()}>
                                    Back
                                </div>
                            </Row> */}
                        </Col>

                        <Col lg={6}>
                            <Row className="d-flex">
                                <div className={"wizard-pages-inactive-btn ml-auto py-2 px-4 mr-3"}>Save Draft</div>
                                
                                <div className={"wizard-pages-active-btn py-2 px-4 mr-3"} onClick={() => goToTrainAgent()}>
                                    {
                                        btnLoading
                                        ?
                                            (<Spinner size="sm" animation="border" variant="light" />)
                                        :
                                            "Next"
                                    }
                                </div>
                            </Row>
                        </Col>
                    </Row>
                </Col>

            </Col>
    
            <Col lg={2} />
        </Row>
    );
}

const mapStateToProps = (state) => ({
    wizardStep: state.wizardR.wizardStep,
    wizardIntentType: state.wizardR.wizardIntentType,
    wizardIdProject: state.wizardR.wizardIdProject,
    wizardExistingProject: state.wizardR.wizardExistingProject,
    wizardExistingProjectIntents: state.wizardR.wizardExistingProjectIntents,
    wizardIntents: state.wizardR.wizardIntents,
    wizardSelectedPage: state.wizardR.wizardSelectedPage,
    wizardSelectedPost: state.wizardR.wizardSelectedPost,
})

export default connect(mapStateToProps, {nextWizardStep,getExistingProject, getAllIntents, trainAgentPage,trainAgentPost })(WizardConfig)
