import React from 'react'
import './AutoWizard.css';

import {
    Row,
    Col,
    Image,
} from 'react-bootstrap';


import WizardSteps from './WizardSteps';
import WizardPages from './WizardPages';
import WizardPosts from './WizardPosts';
import WizardConfig from './WizardConfig';
import WizardTest from './WizardTest';

// Redux:
import { connect } from 'react-redux';
import { nextWizardStep } from '../../../redux/actions/wizardActions';

const INSTA_ICON = require('../../../assets/images/home/insta-icon.png');
const FB_ICON = require('../../../assets/images/home/fb-icon.png');


function AutoWizard(props) {

    return (
        <Col xs={12} style={{height: window.innerHeight * 2}} className="d-flex flex-column" >
        
            <Row className="d-flex">
                <Col lg={1} />
                
                <Col lg={2} className="d-flex flex-column p-0">
                    {/* Wizard Selected Page */}
                    { 
                    props.wizardSelectedPage !== null &&
                    <Row className="d-flex mr-auto px-3 py-1 mb-3">
                        <div className="d-flex flex-column mr-3">
                            <Image src={props.wizardSelectedPage.picture_url} className="m-auto" style={{maxWidth: '45px', borderRadius: '50%'}} />
                            <Image src={props.wizardSelectedPage.platform === "instagram" ? INSTA_ICON : FB_ICON} className="ml-auto" style={{ maxWidth: '16px',zIndex: 1, marginTop: '-10px'}}/>
                        </div>
                        <p className="m-auto wizard-pages-page-title">{props.wizardSelectedPage.name}</p> 
                    </Row>
                    }

                    {/* Wizard Selected Post */}
                    { 
                    props.wizardSelectedPost !== null &&
                    <Row className="d-flex mr-auto px-3 py-2 wizard-post-selection-container">
                        <div className="d-flex flex-column mr-3">
                            <Image src={props.wizardSelectedPost.picture ? props.wizardSelectedPost.picture : props.wizardSelectedPage.picture_url} className="m-auto" style={{maxWidth: '45px'}} />
                        </div>
                        <p className="m-auto wizard-pages-page-title">
                            {props.wizardSelectedPost.message && props.wizardSelectedPost.message.length > 14 
                            ? 
                            props.wizardSelectedPost.message.substring(0,11) + "..." 
                            :
                            props.wizardSelectedPost.message
                            ?
                            props.wizardSelectedPost.message
                            :
                            props.wizardSelectedPage.name
                            }
                        </p>  
                    </Row>
                    }

                </Col>

                <Col lg={5} className="d-flex p-0">
                    <p className="wizard-title m-auto">iModo Wizard</p> 
                </Col>

                <Col lg={4} />
            </Row>

            
            <WizardSteps />
            
            {props.wizardStep === 0 && <WizardPages />}

            {props.wizardStep === 1 && <WizardPosts />}

            {props.wizardStep === 2 && <WizardConfig />}

            {props.wizardStep === 3 && <WizardTest />}

        </Col>
    )
}

const mapStateToProps = (state) => {
    return {
        wizardStep: state.wizardR.wizardStep,
        wizardSelectedPage: state.wizardR.wizardSelectedPage,
        wizardSelectedPost: state.wizardR.wizardSelectedPost,
    }
};

export default connect(mapStateToProps, { nextWizardStep })(AutoWizard);
