import React, { useEffect, useState, } from 'react'
import { connect } from 'react-redux';
import { nextSpecificWizardStep, testAgent } from '../../../redux/actions/wizardActions';

import {
    Row,
    Col,
    Image,
    Spinner,
    OverlayTrigger,
    Popover,
} from 'react-bootstrap';

import {
    Link
} from 'react-router-dom';
import Swal from 'sweetalert2/dist/sweetalert2.js';

const BALISE_ICON = require('../../../assets/images/home/balise.png');
const BALISE_ICON_ACTIVE = require('../../../assets/images/home/balise_active.png');
const EMO_ICON = require('../../../assets/images/home/emo.png');
const EMO_ICON_ACTIVE = require('../../../assets/images/home/emo_active.png');
const LETTER_ICON = require('../../../assets/images/home/3a.png');
const LETTER_ICON_ACTIVE = require('../../../assets/images/home/3a_active.png');
const IMODO_PROFILE_ICON = require('../../../assets/images/home/full-bulle-imodo.png');

// import { FaTrashAlt } from 'react-icons/fa';
// import { IoIosArrowDown } from 'react-icons/io';
// import { AiOutlineCloseCircle } from 'react-icons/ai';
// import { MdAddCircleOutline } from 'react-icons/md';

export const WizardTest = (props) => {

    let {
        wizardIntentType,
        wizardSelectedPage,
        wizardSelectedPost,
    } = props;

    // Popup Config:
    const swalWithBootstrapButtons = Swal.mixin({ customClass: {confirmButton: 'wizard-pages-active-btn-alert'}, buttonsStyling: false });
    

    const [message,setMessage] = useState('');
    const [sendedMsg,setSendedMsg] = useState(null);
    const [btnLoading,setBtnLoading] = useState('');
    
    // Tags:
    const [Tags,setTags] = useState(false);
    const TagsPopover = (
        <Popover id="popover-tags">
          <Popover.Content>
            <Row>
                <Col lg={12}>
                    <Row className="pl-1 my-2">
                        <Col xs={12} className="d-flex mb-1 mt-2">
                            <p className="my-auto mr-auto wizard-config-tag" onClick={() => { setMessage(prevMsg => prevMsg + '##FIRST_NAME##'); }}>First Name</p>
                        </Col>
                         
                        <Col xs={12} className="d-flex mb-1">
                            <p className="my-auto mr-auto wizard-config-tag" onClick={() => { setMessage(prevMsg => prevMsg + '##LAST_NAME##'); }}>Last Name</p>
                        </Col>

                        <Col xs={12} className="d-flex mb-1">
                            <p className="my-auto mr-auto wizard-config-tag" onClick={() => { setMessage(prevMsg => prevMsg + '##FULL_NAME##'); }}  >Full Name</p>
                        </Col>

                        <Col xs={12} className="m-auto d-flex mb-2">
                            <p className="my-auto mr-auto wizard-config-tag" onClick={() => { setMessage(prevMsg => prevMsg + '##PAGE_NAME##'); }}  >Page Name</p>
                        </Col> 
                    </Row>
                </Col>
            </Row>
          </Popover.Content>
        </Popover>
    );

   
    const chat = () => {
        
        props.testAgent(message,wizardIntentType,wizardSelectedPage,wizardSelectedPost)
        .then(() => {
            setSendedMsg(message);
        })
        .catch(err => {
            swalWithBootstrapButtons.fire({
                title: 'Error while finishing the wizard. Try Again!',
                confirmButtonText: 'Okay',
            });
        });
    };

    return (
        <Row className="mt-5">  
            <Col lg={1} />
    
            <Col className="wizard-pages-container mb-3" style={{backgroundColor: 'white'}}>
                
                <Row className="d-flex mt-4 ml-2 mb-4">
                    <p className="my-auto wizard-pages-title">Test Interaction</p>
                </Row>
                
                <Row className="mb-4">
                    <Col lg={9} className="mx-auto" style={{backgroundColor : 'white',borderRadius: '0.2rem',border: '1px solid #B4B4B4', height: '250px' }}>
                        {sendedMsg === null
                        &&
                        <Row className="d-flex" style={{height: '100%'}}>
                            <Col lg={12} className="d-flex" >
                                <p className="m-auto wizard-posts-status-text">Test your interaction...</p>
                            </Col>
                        </Row>

                        }

                        {/* BULLE GRIS */}
                        {sendedMsg !== null &&
                        <Row className="d-flex">
                            <Col>
                                <Row className="my-3">
                                    <Col>
                                        <div className="my-agents-msgs-bulle mb-1">
                                            <p className="my-agents-msgs-bulle-text">
                                               {sendedMsg}
                                            </p>
                                        </div>
                                        {/* <p className="my-agents-msgs-bulle-date">1 hour ago</p> */}
                                    </Col>
                                </Row>
                            </Col>

                            <Col lg={7} />
                        </Row>
                        }

                        {/* {props.wizardReceivedMsg &&
                        <Row className="d-flex">
                            <Col>
                                <Row className="my-3">
                                    <Col>
                                        <div className="my-agents-msgs-bulle mb-1">
                                            <p className="my-agents-msgs-bulle-text">
                                               {props.wizardReceivedMsg}
                                            </p>
                                        </div>
                                     <p className="my-agents-msgs-bulle-date">1 hour ago</p> 
                                    </Col>
                                </Row>
                            </Col>

                            <Col lg={7} />
                        </Row>
                        } */}

                        {/* BULLE BLEU */}
                        {props.wizardReceivedMsg
                        &&
                        <Row className="d-flex">
                            <Col lg={6} />

                            <Col lg={6}>
                                <Row className="my-3">
                                    <Col lg={12}>
                                        <Row className="justify-content-end">
                                            <Col lg={10} className="px-0">
                                                <div className="my-agents-msgs-bulle-bleu mb-1" style={{backgroundColor: '#3B86FF'}}>
                                                    <p className="my-agents-msgs-bulle-text" style={{color: 'white'}}>
                                                        {props.wizardReceivedMsg}
                                                    </p>
                                                </div>
                                            </Col>
                                            <Col lg={2} className="d-flex pl-1">
                                                <Image src={IMODO_PROFILE_ICON} className={"mt-auto"} style={{maxWidth: '50px'}} />
                                            </Col>
                                        </Row>
                                        {/* <p className="my-agents-msgs-bulle-date">1 hour ago</p> */}
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        }

                    </Col>
                </Row>


                <Row className="mt-3 mb-3">
                    <Col lg={9} className="mx-auto" style={{backgroundColor : 'white',borderRadius: '0.2rem',border: '1px solid #B4B4B4'}}>
                        <Row className="pt-4 pb-4">
                            <Col lg={2}>
                                <Row className="justify-content-around">
                                    <div className="d-flex">
                                        <OverlayTrigger trigger="click" placement="top" overlay={TagsPopover}>
                                            <Image 
                                                src={Tags ? BALISE_ICON_ACTIVE : BALISE_ICON} 
                                                onClick={() => setTags(!Tags)} 
                                                className="my-auto mr-auto my-agents-msgs-tiny-icon" 
                                                style={{ width: '19px', cursor: 'pointer'}} 
                                            />   
                                        </OverlayTrigger> 
                                        {/* <Image src={BALISE_ICON} className="m-auto my-agents-msgs-tiny-icon" />     */}
                                    </div>                                

                                    <div className="d-flex">
                                        <Image src={EMO_ICON} className="m-auto my-agents-msgs-tiny-icon" />
                                    </div>

                                    <div className="d-flex">
                                        <Image src={LETTER_ICON} className="m-auto my-agents-msgs-tiny-icon" />
                                    </div>
                                </Row>
                            </Col>

                            <Col lg={10} className="d-flex p-0">
                                <input 
                                    type="text" 
                                    className="my-auto wizard-test-input w-100" 
                                    placeholder="Your Message..." 
                                    onChange={(e) => setMessage(e.target.value)} value={message}
                                    onKeyDown={(e) => e.key === 'Enter' && chat()}
                                />
                            </Col>

                            <Col lg={3} />    

                        </Row>
                    </Col>
                </Row>


                <Row className="d-flex justify-content-between pl-4 pr-4">
                    <div className="wizard-pages-inactive-btn py-2 px-4 mb-3">
                        {/* MUST RESET ALL WIZARD VARS */}
                        <Link to="/home/pages" style={{textDecoration: 'none', color: '#818E94'}} onClick={() => props.nextSpecificWizardStep(2)} >
                            Back
                        </Link>
                    </div>

                    <div className="mb-3">
                        <Row>
                            <div className="wizard-pages-inactive-btn py-2 px-4 mr-3">
                                Save Draft
                            </div>

                            <div className={"wizard-pages-active-btn py-2 px-4 mr-3"}>
                                {
                                btnLoading
                                ?
                                (<Spinner size="sm" animation="border" variant="light" />)
                                :
                                "Finish"
                                }
                            </div>
                        </Row>
                    </div>
                </Row>

            </Col>
    
            <Col lg={1} />
        </Row>
        )
}

const mapStateToProps = (state) => ({
    wizardStep: state.wizardR.wizardStep,
    wizardIntentType: state.wizardR.wizardIntentType,
    wizardSelectedPage: state.wizardR.wizardSelectedPage,
    wizardSelectedPost: state.wizardR.wizardSelectedPost,
    wizardReceivedMsg: state.wizardR.wizardReceivedMsg,
    // wizardIdProject: state.wizardR.wizardIdProject,
})

export default connect(mapStateToProps, { nextSpecificWizardStep, testAgent })(WizardTest)


/*

{props.wizardReceivedMsg &&
                        <Row className="d-flex">
                            <Col>
                                <Row className="my-3">
                                    <Col>
                                        <div className="my-agents-msgs-bulle mb-1">
                                            <p className="my-agents-msgs-bulle-text">
                                               {props.wizardReceivedMsg}
                                            </p>
                                        </div>
                                        <p className="my-agents-msgs-bulle-date">1 hour ago</p>
                                        </Col>
                                        </Row>
                                    </Col>
        
                                    <Col lg={7} />
                                </Row>
                                }

*/