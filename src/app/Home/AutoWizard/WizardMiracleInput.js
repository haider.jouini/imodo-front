import React, {useState} from 'react';


import {
    Row,
    Col,
    Image,
    OverlayTrigger,
    Popover,
    Form,
} from 'react-bootstrap';
import Picker from 'emoji-picker-react';
import Keyboard from 'react-simple-keyboard';
import layout from "simple-keyboard-layouts/build/layouts/arabic";
import ReactTooltip from 'react-tooltip';

import { IoIosCall } from 'react-icons/io';
import { AiOutlineCloseCircle } from 'react-icons/ai';
import { BsLink45Deg } from 'react-icons/bs';


// Icons:
const BALISE_ICON = require('../../../assets/images/home/balise.png');
const BALISE_ICON_ACTIVE = require('../../../assets/images/home/balise_active.png');
const EMO_ICON = require('../../../assets/images/home/emo.png');
const EMO_ICON_ACTIVE = require('../../../assets/images/home/emo_active.png');
const LETTER_ICON = require('../../../assets/images/home/3a.png');
const LETTER_ICON_ACTIVE = require('../../../assets/images/home/3a_active.png');



export default function WizardMiracleInput(props) {
    const [BtnsPop, setBtnPop] = useState(false);

    let {
        label,
        response,
        handleResponse,
        showTags,
        handleTags,
        showEmojis,
        handleEmojis,
        showKeyboard,
        handleKeyboard,
        BtnType,
        handleBtnType,
        BtnTitle,
        handleBtnTitle,
        BtnValue,
        handleBtnValue,
        Btns,
        handleBtns,
    } = props;

    // render Functions :
    const Tags = (
        <Popover id="popover-tags">
          <Popover.Content>
            <Row>
                <Col lg={12}>
                    {/* <Row className="d-flex w-100">
                        <IoIosClose className="ml-auto my-auto pt-1" color={'#9F9F9F'} size={30} onClick={() => { handleTags(false) }} style={{cursor: 'pointer'}} />
                    </Row> */}

                    <Row className="pl-1 my-2">
                        <Col xs={12} className="d-flex mb-1 mt-2">
                            <p className="my-auto mr-auto wizard-config-tag" onClick={() => { handleResponse(prevResponse => prevResponse + '##FIRST_NAME##'); }}>First Name</p>
                        </Col>
                         
                        <Col xs={12} className="d-flex mb-1">
                            <p className="my-auto mr-auto wizard-config-tag" onClick={() => { handleResponse(prevResponse => prevResponse + '##LAST_NAME##'); }}>Last Name</p>
                        </Col>

                        <Col xs={12} className="d-flex mb-1">
                            <p className="my-auto mr-auto wizard-config-tag" onClick={() => { handleResponse(prevResponse => prevResponse + '##FULL_NAME##'); }}  >Full Name</p>
                        </Col>

                        <Col xs={12} className="m-auto d-flex mb-2">
                            <p className="my-auto mr-auto wizard-config-tag" onClick={() => { handleResponse(prevResponse => prevResponse + '##PAGE_NAME##'); }}  >Page Name</p>
                        </Col> 
                    </Row>
                </Col>
            </Row>
          </Popover.Content>
        </Popover>
    );

    const BtnsPopover = (
        <Popover show={BtnsPop}  id="popover-quick-replies">
          <Popover.Content>
            <Row className="justify-content-center">
                <Col md={12}>
                    <Row>
                        <Col md={12} className="mt-2 mb-3">
                            <Form.Control as="select" onChange={(e) => handleBtnType(e.target.value)} defaultValue={BtnType}>
                                <option value="url">URL</option>
                                <option value="call">CALL</option>
                            </Form.Control>
                        </Col>
                        
                        <Col md={12} className="mt-2">
                            <Row className="d-flex">
                                <Col md={12}>
                                    <Form.Control 
                                        type={"text"} 
                                        onChange={(e) => handleBtnTitle(e.target.value)} 
                                        value={BtnTitle}  
                                        placeholder="Title" 
                                    />
                                </Col>

                                <Col md={12} className="d-flex">
                                    <p className="ml-auto wizard-config-input-feedback">Remaining characters: 20</p>
                                </Col>
                            </Row>
                        </Col>

                        <Col md={12} className="mb-2">
                            <Form.Control 
                                type={BtnType === "url" ? "text" : "number"} 
                                onChange={(e) => handleBtnValue(e.target.value)} 
                                value={BtnValue} 
                                placeholder={BtnType === "url" ? "www.example.com" : "(+216) 99 999 999"} 
                            />
                        </Col>
                    </Row>
                </Col>

                <Col md={11} className="d-flex pl-1">
                    {/* <div className="wizard-pages-active-btn py-3 px-5" onClick={() => addNewQuickReply()}>
                        OK
                    </div> */}
                    <div className="wizard-pages-active-btn py-2 px-4" onClick={() => addNewBtn()}>
                        OK
                    </div>
                </Col>   
            </Row>
                
          </Popover.Content>
        </Popover>
    );

    // Functions :
    const addNewBtn = () => {
        let newBtns = [...Btns];
        
        let BtnToPush = {
            type: BtnType,
            title: BtnTitle,
            value: BtnValue,
        };
        newBtns.push(BtnToPush);

        // update new Btns :
        handleBtns(newBtns);

        // reset
        handleBtnType('url');
        handleBtnTitle('');
        handleBtnValue('');
        setBtnPop(false);
    };

    const deleteBtn = (index) => {
        let newBtns = [...Btns];
        newBtns.splice(index,1);
        handleBtns(newBtns);
    };

    return (
        <Row className="mx-0 px-2">

            <Col lg={12} className="mx-0 px-0">
                <label className="wizard-config-textarea-label">{label}</label>
            </Col>

            <Col lg={12} className="wizard-intent-miracle-box px-1">
            
                <Row className="mb-1 mx-0 pt-1 px-2">
                    <Col lg={1} className="d-flex p-0">
                        <OverlayTrigger trigger="click" placement="top" overlay={Tags}>
                            <Image 
                                src={showTags ? BALISE_ICON_ACTIVE : BALISE_ICON} 
                                onClick={() => handleTags(!showTags)} 
                                className="my-auto mr-auto my-agents-msgs-tiny-icon" 
                                style={{ width: '19px', cursor: 'pointer'}} 
                            />   
                        </OverlayTrigger> 
                    </Col>                                

                    <Col lg={1} className="d-flex p-0">
                        <Image 
                            src={showEmojis ? EMO_ICON_ACTIVE : EMO_ICON} 
                            onClick={() => handleEmojis(!showEmojis)} 
                            className="my-auto mr-auto my-agents-msgs-tiny-icon" 
                            style={{ width: '18px', cursor: 'pointer'}} 
                        />
                    </Col>
        
                    <Col lg={1} className="d-flex p-0">
                        <Image 
                            src={showKeyboard ? LETTER_ICON_ACTIVE : LETTER_ICON} 
                            onClick={() => handleKeyboard(!showKeyboard)} 
                            className="my-auto mr-auto my-agents-msgs-tiny-icon" 
                            style={{ width: '21px', cursor: 'pointer'}} 
                        />
                    </Col>
                
                    <Col lg={8}>
                        {showKeyboard &&
                            <div className="wizard-config-keyboard-container">
                                <Keyboard 
                                    onChange={(input) => handleResponse(prevResponse => prevResponse + input.charAt(input.length - 1))} 
                                    // onKeyPress={(button) => onKeyPress(button,"generic")} 
                                    layout={layout} 
                                />
                            </div>
                        }

                        {showEmojis &&
                            <div className="wizard-config-emoji-container" >
                                <Picker onEmojiClick={(e,obj) => handleResponse(prevResponse=> prevResponse + obj.emoji) } />
                            </div>
                        } 
                    </Col>
                </Row>

                <Row>
                    <Col lg={12} className="mx-0">
                        <textarea 
                            onChange={(e) => handleResponse(e.target.value)} 
                            placeholder={"Your Message..."}
                            // onFocus={() => console.log("Hey I'm Focused")} 
                            value={response} 
                            className="wizard-config-textarea w-100 py-0" 
                            rows="5"
                        />
                    </Col>
                </Row>

                <Row >
                    <Col lg={12} className="my-0 py-0">
                        <hr style={{height: '2px', borderColor: '#B4B4B4', margin: '1px'}} />
                    </Col>
                </Row>

                
                <Row className="mx-0">
                    <Col lg={12}>
                        
                        <Row>
                            {Btns.map((btn,index) => 
                                <Col key={index} lg={3} className="mr-2">
                                    <Row className="wizard-config-quick-replies-btn my-1 py-1 d-flex">
                                        {btn.type === "url" 
                                        ? 
                                        <BsLink45Deg color={'#fff'} size={15} className="ml-1 mr-auto my-auto"/>
                                        :
                                        <IoIosCall color={'#fff'} size={15} className="ml-1 mr-auto my-auto"/>
                                        }
                                        <p className={btn.type === "url" ? "mr-auto my-auto" : "ml-1 mr-auto my-auto" } data-for='quickReplyTip' data-tip={btn.value}>{btn.type}</p>
                                        <AiOutlineCloseCircle className="ml-auto my-auto mr-1" color={"white"} size={"15"} style={{cursor: 'pointer'}} onClick={() => deleteBtn(index)} />
                                    </Row>
                                    <ReactTooltip id='quickReplyTip' textColor='#fff' backgroundColor='#009EE3' />
                                </Col>
                            )}
                            {Btns.length < 3
                            &&
                            <OverlayTrigger trigger="click" placement="top" overlay={BtnsPopover}>
                                <Col lg={3} className="wizard-config-quick-replies-btn my-1 py-1 d-flex">
                                    <p className="my-auto">Add button</p>
                                </Col>
                            </OverlayTrigger>
                            }
                        </Row>
                    </Col>
                </Row> 
               
            </Col>
        </Row>
    )
}
