import React, { useEffect } from 'react'

import Pages from '../Pages/Pages';

import {
    // Col,
    Row,
    Table,
    Image,
} from 'react-bootstrap';

import {
    Link
} from 'react-router-dom';

import { connect } from 'react-redux';
import {  getConnectedPagesProject, resetSocialMediaSelections } from '../../../redux/actions/socialMediaActions';


const PROJECT_PHOTO = require('../../../assets/images/home/project-photo.png');
const EDIT_ICON = require('../../../assets/images/home/edit-icon.png');
const MSG_ICON = require('../../../assets/images/home/message-icon.png');
const TRASH_ICON = require('../../../assets/images/home/trash-icon.png');


function MyAgents(props) {

    const tableTitles = [
        {title : 'Picture'},
        {title : 'Project name'},
        {title : 'Date'},
        {title : 'Status'},
        {title : 'Edit project'},
    ];

    useEffect(() => {
        props.resetSocialMediaSelections();
    },[]);

    useEffect(() => {
        // setLoading(true);
        props.getConnectedPagesProject();
    }, [props.socialMediaPageSelected])

  
    return (
    <div style={{ height: window.innerHeight * 1.2 }}>
        <Pages />

        {props.socialMediaPageSelected &&
            <>
                <Row className="mt-5">
                    <p className="my-agents-table-label">Liste des projets</p>
                </Row>
                
                <Row className="mb-5">

                    <Table responsive="lg" variant="light">
                        <thead>
                        <tr>
                            {
                                tableTitles.map((item,index) => (
                                    <>
                                    <th key={index} className="my-agent-table-title">{item.title}</th>
                                    {
                                        index < tableTitles.length - 1
                                        &&
                                        <>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        </>   
                                    }
                                    </>    
                                ))
                            }
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td className="d-flex">
                                <div className="m-auto">
                                    <Image src={PROJECT_PHOTO} className={"m-auto"} />
                                </div>    
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td className="d-flex">
                                <div className="m-auto" >
                                    <p className="m-auto my-agent-project-name">GMC_Hack</p>
                                </div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                            <div className="flex-column">
                                    <p className="m-auto my-agent-project-date">04/02/2020</p>
                                    <p  className="m-auto my-agent-project-time">11:45 PM</p>
                            </div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td className="d-flex">
                                <div className="m-auto">
                                    <div className="my-agent-status-btn">
                                        Active
                                    </div>
                                </div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td className="d-flex">
                                <div className="m-auto">
                                    <Image src={EDIT_ICON} className={"mr-auto my-agents-icon"} />
                                    <Link to="/home/my-agents-messages"><Image src={MSG_ICON} className={"ml-2 mr-2 my-agents-icon"} /></Link>
                                    <Image src={TRASH_ICON} className={"ml-auto my-agents-icon"} />
                                </div>
                            </td>
                        </tr>
                        
                        </tbody>
                    </Table> 
                </Row>
            </>
        }

    </div>
    );
};

const mapStateToProps = (state) => {
    return {
        fbData : state.socialMediaR.fbData,
        instaData : state.socialMediaR.instaData,
        socialMediaPageSelected : state.socialMediaR.socialMediaPageSelected,
    }
};

export default connect(mapStateToProps, { getConnectedPagesProject, resetSocialMediaSelections })(MyAgents);

