import React from 'react';

import  {
    Col,
    Row,
    Image,
    Form
} from 'react-bootstrap';

import { BiSearch,BiShare } from 'react-icons/bi'; 
import { IoIosClose } from 'react-icons/io'; 
import { AiOutlineEllipsis, AiFillClockCircle } from 'react-icons/ai'; 
import { MdPhone, MdEmail } from 'react-icons/md'; 
import { FaBirthdayCake, FaHome } from 'react-icons/fa'; 
import { RiMapPin2Fill } from 'react-icons/ri'; 

const PROFILE_PHOTO = require('../../../assets/images/home/profile-photo.png');
const LETTER_ICON = require('../../../assets/images/home/3a.png');
const EMO_ICON = require('../../../assets/images/home/emo.png');
const BALISE_ICON = require('../../../assets/images/home/balise.png');


export default function MyAgentsMessages() {
    return (
        <>
           <Row>
               <p className="my-agents-msgs-title" >Liste des messages réçus</p>
           </Row>

           <Row className="mb-3">
               <Col lg={3}>
                    <Row className="justify-content-center">
                        <div className="my-agents-msgs-matched-btn">Matched</div>
                        <div className="my-agents-msgs-not-matched-btn">Not Matched</div>
                    </Row>
               </Col>

               <Col />
           </Row>
           

           <Row>
                {/* BLOC 1 */}
                <Col lg={3} className="pt-3 pb-5" style={{backgroundColor : 'white',borderRadius: '0.5rem'}}>
                    <Row className="d-flex">
                        <BiSearch color={"#BCBCCB"} size={"20"} className="my-auto mr-2"  />
                        <Form.Control type="text" className="my-auto my-agents-search-input" placeholder="Search Message or Name…" />
                    </Row>

                    {/* MSG */}
                    <div className="pt-1 mt-3">
                        <Row className="d-flex p-4" style={{backgroundColor: '#F2F2F2'}}>
                            <Row className="mr-auto d-flex">
                                <Image src={PROFILE_PHOTO} className={"m-auto"} style={{maxWidth: '50px'}} />
                                <p className="my-auto ml-2 my-agents-msg-name">Enis</p>
                            </Row>

                            <Row className="ml-auto d-flex">
                                <p className="m-auto my-agents-msg-date">04/02/2020 11:15PM</p>
                            </Row>
                        </Row>
                        
                        <Row className="d-flex pl-5 pr-5" style={{backgroundColor: '#F2F2F2'}}>
                            <p className="m-auto my-agents-msg-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit…</p>
                        </Row>
                        
                        <Row className="d-flex p-4" style={{backgroundColor: '#F2F2F2'}} >
                            <Row className="mr-auto d-flex">
                                <p className="my-auto my-agents-msg-intent">95% Intent: Prix</p>
                            </Row>

                            <Row className="ml-auto d-flex">
                                <input type="checkbox" className="my-auto" />
                            </Row>
                        </Row>
                    </div>
                    
                    {/* MSG */}
                    <div className="pt-1">
                        <Row className="d-flex p-4" style={{backgroundColor: 'white'}}>
                            <Row className="mr-auto d-flex">
                                <Image src={PROFILE_PHOTO} className={"m-auto"} style={{maxWidth: '50px'}} />
                                <p className="my-auto ml-2 my-agents-msg-name">Enis</p>
                            </Row>

                            <Row className="ml-auto d-flex">
                                <p className="m-auto my-agents-msg-date">04/02/2020 11:15PM</p>
                            </Row>
                        </Row>
                        
                        <Row className="d-flex pl-5 pr-5" style={{backgroundColor: 'white'}}>
                            <p className="m-auto my-agents-msg-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit…</p>
                        </Row>
                        
                        <Row className="d-flex p-4" style={{backgroundColor: 'white'}} >
                            <Row className="mr-auto d-flex">
                                <p className="my-auto my-agents-msg-intent">95% Intent: Prix</p>
                            </Row>

                            <Row className="ml-auto d-flex">
                                <input type="checkbox" className="my-auto" />
                            </Row>
                        </Row>
                    </div>
                    
                </Col>
                
                {/* BLOC 2 */}
                <Col lg={5} className="mr-auto ml-auto" style={{backgroundColor : 'white',borderRadius: '0.5rem'}}>
                    <Row className="d-flex p-4" style={{backgroundColor: 'white'}}>
                        <Row className="mr-auto d-flex">
                            <Image src={PROFILE_PHOTO} className={"m-auto"} style={{maxWidth: '50px'}} />
                            <p className="my-auto ml-2 my-agents-msg-name">Enis</p>
                        </Row>

                        <Row className="ml-auto d-flex">
                            <BiShare color={"#E5007D"} size={"32"}  className="my-auto mr-3" />
                            <IoIosClose color={"#A4AFB7"} size={"28"} className="my-auto" />
                        </Row>
                    </Row>

                    {/* BULLE GRIS */}
                    <Row className="d-flex">
                        <Col>
                            <Row className="mt-5">
                                <Col lg={2} className="d-flex">
                                    <Image src={PROFILE_PHOTO} className={"mt-auto"} style={{maxWidth: '50px'}} />
                                </Col>

                                <Col>
                                    <div className="my-agents-msgs-bulle">
                                        <p className="my-agents-msgs-bulle-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
                                    </div>
                                    <p className="my-agents-msgs-bulle-date">1 hour ago</p>
                                </Col>
                            </Row>
                        </Col>

                        <Col lg={2} />
                    </Row>
                   
                   {/* BULLE BLEU */}
                   <Row className="d-flex">
                        <Col lg={2} />

                        <Col>
                            <Row className="mt-5">
                                <Col className="d-flex flex-column">
                                    <div className="my-agents-msgs-bulle-bleu">
                                        <p className="my-agents-msgs-bulle-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
                                    </div>
                                    <p className="my-agents-msgs-bulle-date ml-auto">1 hour ago</p>
                                </Col>

                                <Col lg={2} className="d-flex">
                                    <Image src={PROFILE_PHOTO} className={"mt-auto"} style={{maxWidth: '50px'}} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>


                    <Row className="mt-5">
                        <Col lg={3}>
                            <Row className="justify-content-around">
                                <div className="d-flex">
                                    <Image src={BALISE_ICON} className="m-auto my-agents-msgs-tiny-icon" />    
                                </div>                                

                                <div className="d-flex">
                                    <Image src={EMO_ICON} className="m-auto my-agents-msgs-tiny-icon" />
                                </div>

                                <div className="my-agents-msgs-tiny-box d-flex">
                                    <Image src={LETTER_ICON} className="m-auto my-agents-msgs-tiny-icon" />
                                </div>
                            </Row>
                        </Col>

                        <Col lg={6} className="d-flex p-0">
                            <input type="text"  className="my-auto" placeholder="Your Message..." />
                        </Col>

                        <Col lg={3} />
                    </Row>

                </Col>


                {/* BLOC 3 */}
                <Col lg={3} style={{backgroundColor : 'white'}}>

                    <Row>
                    <Col lg={10}>
                    
                        <Row className="d-flex p-4" style={{backgroundColor: 'white'}}>
                            <Row className="mr-auto d-flex">
                                <Image src={PROFILE_PHOTO} className={"m-auto"} style={{maxWidth: '50px'}} />
                                <p className="my-auto ml-2 my-agents-msg-name">Enis</p>
                            </Row>

                            <Row className="ml-auto d-flex">
                                <AiOutlineEllipsis color={"#A4AFB7"} size={"32"} className="my-auto" />
                            </Row>
                        </Row>


                        <Row className="pl-3">
                            <p className="my-agents-profile-black-text">About</p>
                        </Row>

                        <Row className="pl-3 pr-3 justify-content-between">
                            <p className="my-agents-profile-gray-text">Added details</p>
                            <p className="my-agents-profile-blue-text">Edit</p>
                        </Row>

                        <Row className="pl-3 d-flex mb-3">
                            <MdPhone color={"#B4B4B4"} size={"25"} className="my-auto mr-3" />
                            <p className="my-auto my-agents-profile-black-text">+ Phone number</p>
                        </Row>
                        
                        <Row className="pl-3 d-flex mb-3">
                            <MdEmail color={"#B4B4B4"} size={"25"} className="my-auto mr-3" />
                            <p className="my-auto my-agents-profile-black-text">+ Email</p>
                        </Row>
                        
                        <Row className="pl-3 d-flex mb-3">
                            <FaBirthdayCake color={"#B4B4B4"} size={"25"} className="my-auto mr-3" />
                            <p className="my-auto my-agents-profile-black-text">16 Avril</p>
                        </Row>
                        
                        <Row className="pl-3 d-flex mb-3">
                            <FaHome color={"#B4B4B4"} size={"25"} className="my-auto mr-3" />
                            <p className="my-auto my-agents-profile-black-text">+ Adresse</p>
                        </Row>

                        <Row className="pl-3 pr-3 justify-content-between">
                            <p className="my-agents-profile-gray-text">Facebook</p>
                            <p className="my-agents-profile-blue-text">View profile</p>
                        </Row>

                        <Row className="pl-3">
                            <AiFillClockCircle color={"#B4B4B4"} size={"25"} className="mr-3" />
                            <p>Local time 16:16 PM</p>
                        </Row>

                        <Row className="pl-3">
                            <RiMapPin2Fill color={"#B4B4B4"} size={"25"} className="mr-3" />
                            <p className="my-agents-profile-blue-text">Tunis, Tunisia</p>
                        </Row>
                    </Col>

                    <Col lg={2} style={{backgroundColor: '#F9F9F9'}} />
                    </Row>
                    
                </Col>

           </Row>
        </>
    )
}
