import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { resetSocialMediaSelections, inviteMember, deleteTeamMember, sendMailTeam } from '../../../redux/actions/socialMediaActions';
import Pages from '../Pages/Pages';

import {
    Col,
    Row,
    Modal,
    Spinner,
    Image,
    Form
} from 'react-bootstrap';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import Lottie from 'react-lottie';

import { CopyToClipboard } from 'react-copy-to-clipboard';


import notFoundAnimationData from '../../../assets/json/not-found.json';
import { FaTrashAlt, FaInfoCircle } from 'react-icons/fa';
import { IoIosCopy } from 'react-icons/io';

const GRAY_SEPERATOR = require('../../../assets/images/home/gray-separator.png');

export const Team = (props) => {

    const fbHost = "https://graph.facebook.com/v8.0";
    // Popup Config: 
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'wizard-pages-active-btn',
        },
        buttonsStyling: false
    });

    // Lottie config
    const defaultOptionsNotFound = {
        loop: true,
        autoplay: true, 
        animationData: notFoundAnimationData,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice'
        }
    };
   
    const [showPopover, setShowPopover] = useState(false);
    const [showInviteModal, setShowInviteModal] = useState(false);
    const [loadingInvite, setLoadingInvite] = useState(false);
    const [inviteToken, setInviteToken] = useState('');
    const [teamMembers, setTeamMembers] = useState([]);
    const [emailTeamMember,setEmailTeamMember] = useState(''); 

    const handleInviteModal = () => setShowInviteModal(!showInviteModal);

    const handlePopover = (delay) => {
        setTimeout( () => {
            setShowPopover(!showPopover);
        },delay);
    };

    const validateEmail = (mail) => {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail)) {
            return (true);
        } else {
            return (false);
        }
    };
    
    const renderPopover = () => (
        <div className="d-flex manage-products-popover">
          <p className="m-auto">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
          </p>
        </div>
    );

    const renderModal = (page) => (
        <Modal show={showInviteModal} onHide={handleInviteModal} size="lg" centered>
            <div className="d-flex flex-column">
                <Modal.Header closeButton>
                    <Modal.Title className="manage-product-entity-name">Add Member</Modal.Title>
                </Modal.Header>
                {/* <IoIosClose className="ml-auto mr-3 mt-2" color={'#9F9F9F'} size={'28'} onClick={handleInviteModal} style={{cursor: 'pointer'}} /> */}
                
                <Modal.Body>
                    <Row className="d-flex">
                        <Col lg={12} className="d-flex flex-column">
                            <p className="m-auto manage-product-entity-name">Copy the invitation link below and send it to your team member.</p>
                            <p className="m-auto manage-product-entity-name">Please note that this link is one-time only and is only valid for 24 hours.</p>
                        </Col>
                    </Row>

                    {/* <p className="m-auto team-token-link">Token of invitation:</p> */}
                    <Row className="d-flex my-3">
                        <Col md={1}/>
                        
                        <Col md={10} className="d-flex">
                            <Col md={11} className="team-token-container p-2 mx-0">
                                <p className="my-auto team-token-link">{`${window.location.origin}/${inviteToken.substring(0,33)}...`}</p>
                            </Col>
                            <Col md={1} className="d-flex p-0">
                                <CopyToClipboard 
                                    text={`${window.location.origin}/${inviteToken}`}
                                    onCopy={() => {
                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: 'Successfully copied to your clipboardd',
                                            showConfirmButton: false,
                                            timer: 1500
                                        })
                                    }}
                                >
                                <span className="d-flex team-copy-span mr-auto" style={{ cursor: 'pointer' }}><IoIosCopy className="m-auto" size={"30"} color={"#fff"} /></span>
                                </CopyToClipboard>
                            </Col>
                        </Col>

                        <Col lg={1} />
                    </Row>



                    <Row className="justify-content-center my-2">
                        <Col md={6} className="d-flex">
                            <Image src={GRAY_SEPERATOR} fluid />
                        </Col>
                    </Row>
                    
                    <Row className="justify-content-center my-2">
                        <Col md={8} className="d-flex">
                            <p className="manage-product-entity-name m-auto">Or enter the email address of your team member</p>
                        </Col>
                    </Row>

                    <Row className="justify-content-center my-2">
                        <Col md={4} className="d-flex">
                            <Form.Control type={"text"} className="ml-auto my-auto" value={emailTeamMember} onChange={(e) => setEmailTeamMember(e.target.value) } placeholder={"Email@email.com"} />
                        </Col>

                        <Col md={3} className="d-flex">
                            <div 
                                className={"wizard-pages-active-btn py-2 px-4 mr-3 text-center mr-auto my-auto"} 
                                onClick={() => {
                                    props.sendMailTeam(props.socialMediaPageSelected,emailTeamMember)
                                    .then(() => {
                                        setShowInviteModal(false);
                                    })
                                    .catch((err) => console.log("Err modal send invitation Team", err))
                                }} 
                            >
                                Send invitation
                            </div>
                        </Col>
                    </Row>
                    
                </Modal.Body>

            
            </div>
        </Modal>
    );

    const deleteMember = async (indexMember) => {
        let oldMembers = [...teamMembers];
        oldMembers.splice(indexMember,1);
        setTeamMembers(oldMembers);
    };

    useEffect(() => {
        props.resetSocialMediaSelections();
    },[]);

    useEffect(() => {
        if(props.socialMediaPageSelected) {
            setTeamMembers(props.socialMediaPageSelected.team);
        };
    },[props.socialMediaPageSelected]);

    return (
        <div style={{height: window.innerHeight * 1.2}}>

            {renderModal(props.socialMediaPageSelected)}

            <Row className="mt-5">
                <p className="my-agents-table-label" style={{ fontSize: '30px', fontFamily: 'Poppins SemiBold' }}>Team</p>
                <FaInfoCircle color={"#4080FF"} size={'15'} onMouseEnter={() => handlePopover('20')} onMouseLeave={() => handlePopover('300')} />
                { showPopover && renderPopover() }
            </Row>

            <Pages title="Select a page to manage team" titleStyling={{color: '#4D4F5C', fontSize: '16px', fontFamily: 'Poppins Medium' }} />

            {props.socialMediaPageSelected
            &&
            <>
            {props.socialMediaPageSelected.status !== "listPageConnectedOtherTeam"
            ?
            <Row className="mt-5 mr-0 d-flex flex-column">                
                
                {props.socialMediaPageSelected.status === "listPageConnectedOwner"
                &&
                <Row className="mb-3"> 
                    <Col lg={12} className="d-flex">
                        <div 
                            className="team-invite-btn mr-auto d-flex" 
                            onClick={() => {
                                setLoadingInvite(true);
                                props.inviteMember(props.socialMediaPageSelected)
                                .then((res) => {
                                    setInviteToken(res.data.data);
                                    setTimeout(() => {
                                        setLoadingInvite(false);
                                        setShowInviteModal(true);
                                    },1500);
                                })
                                .catch(() => {
                                    swalWithBootstrapButtons.fire({
                                        title: 'Error while generating the token',
                                        confirmButtonText: 'Retry',
                                    });
                                });    
                            }}
                        >
                            {loadingInvite 
                            ?
                            (<>Generating token <Spinner animation="border" size="sm" variant="primary" className="spinnerWhite my-auto ml-2" /></>)
                            :
                            "Invite Member"
                            }
                        </div>
                    </Col>
                </Row>
                }

                <Col lg={10} className="p-3" style={{backgroundColor: props.socialMediaPageSelected.team.length ? 'white' : 'transparent', borderRadius: '0.3rem'}}>
                    {teamMembers.length
                    ?
                    <>

                    <Row>
                        <Col lg={4} className="d-flex pt-3 pb-3" >
                            <p className="mr-auto my-auto manage-products-table-title">Name</p>
                        </Col>
                        
                        <Col lg={4} className="d-flex pt-3 pb-3">
                            <p className="m-auto manage-products-table-title">Role</p>
                        </Col>

                        <Col lg={4} className="d-flex pt-3 pb-3">
                            <p className="ml-auto my-auto manage-products-table-title">Actions</p>
                        </Col>
                    </Row>

                    {teamMembers.map((member,indexMember) =>     
                    <Row key={member.idFacebook+Math.random()}>
                        <Col lg={4} className="d-flex pt-3 pb-3">
                            <p className="my-auto manage-product-entity-name">{member.first_name} {member.last_name}</p>
                        </Col>
                        
                        <Col lg={4} className="d-flex pt-3 pb-3">
                            <p className="m-auto manage-product-entity-name">{member.role.toUpperCase()}</p>
                        </Col>

                        <Col lg={4} className="d-flex pt-3 pb-3">
                            {
                            props.socialMediaPageSelected.status === "listPageConnectedOwner"
                            &&
                            <FaTrashAlt 
                                className="ml-auto my-auto mr-3" 
                                color={ member.role === "owner" ? 'gray' : '#E5007D'} 
                                size={'25'} 
                                style={{ cursor: 'pointer' }} 
                                onClick={() => {

                                    if(member.role === "owner") {
                                        swalWithBootstrapButtons.fire({
                                            title: "You can't delete the owner",
                                            confirmButtonText: 'Okay',
                                        });
                                    } else {
                                        props.deleteTeamMember(props.socialMediaPageSelected, member.idFacebook)
                                        .then(() =>{
                                            deleteMember(indexMember);
                                        })
                                        .catch(() => {
                                            swalWithBootstrapButtons.fire({
                                                title: 'Error while deleting the member',
                                                confirmButtonText: 'Retry',
                                            });
                                        })
                                    }
                                }} 
                            />
                            }
                        </Col>
                    </Row>
                    )}
                    </>
                    :
                    <>
                    <Col lg={12} className="d-flex" style={{backgroundColor: '#F9F9F9' }}>
                        <Lottie options={defaultOptionsNotFound} width={200} className="m-auto" /*height={400}*/ />
                    </Col>

                    <Col lg={12} className="d-flex mt-5" style={{backgroundColor: '#F9F9F9' }}>
                        <p className="m-auto home-big-title">Sorry ! You don't have any team member.</p>
                    </Col>
                    </>
                    }
                
                </Col>
                        
                <Col lg={2} />
            </Row>
            :
            <>
            <Col lg={12} className="d-flex" style={{backgroundColor: '#F9F9F9' }}>
                <Lottie options={defaultOptionsNotFound} width={200} className="m-auto" /*height={400}*/ />
            </Col>

            <Col lg={12} className="d-flex mt-5" style={{backgroundColor: '#F9F9F9' }}>
                <p className="m-auto home-big-title">Sorry ! You can't see this team</p>
            </Col>
            </>
            }
            </> 
            }
        
        </div>
    )
}

const mapStateToProps = (state) => ({
    socialMediaPageSelected : state.socialMediaR.socialMediaPageSelected,
})

export default connect(mapStateToProps, { resetSocialMediaSelections,inviteMember, deleteTeamMember, sendMailTeam })(Team)
