import React, { useEffect } from 'react'
import './Login.css';
import Facebook from './Facebook';

// Components :
import Navbar from '../Components/Navbar';

import {
    Container,
    Row,
    Col,
    Image,
    // Nav,
} from 'react-bootstrap';

import {
    Link,
    Redirect,
} from 'react-router-dom';

// Icons & Images:
const LANDING_IMG = require('../../assets/images/login/landing-img.png');
const MIND_ICON = require('../../assets/images/login/mind.png');
const CHATBOT_ICON = require('../../assets/images/login/chatbot.png');
const CORE_AI_ICON = require('../../assets/images/login/coreai.png');


function LandingPage() {

    const auth_token = localStorage.getItem('authtoken');
    const userData = JSON.parse(localStorage.getItem('userData'));

    const allMenuItems = [
        { title: 'What is Imodo' , route: 'link-what-is' },
        { title: 'How to use it' , route: 'link-how-to-use' },
        { title: 'For developpers' , route: 'link-for-devs' }
    ];
  
 
    return (
        <>
        { auth_token !== null && userData !== null && <Redirect to="/home/pages" /> }
        <Navbar menuItems={allMenuItems} loginBtn={true} />

        <Container fluid>
            {/* BLOC 1 */}
            <Row className="d-flex mb-5" style={{background: "#F9F9F9"}}>
                <Col lg={2} className={"mt-5"} />

                <Col lg={4} className={"d-flex flex-column justify-content-around p-5 landing-texts-container"}>
                    {/* <div className="m-auto d-flex justify-content-between flex-column"> */}
                        <p className="landing-big-text">Conversation is <br/> a first step of a business </p>
                        <p className="landing-desc">it’s analysis is a first step to business</p>
                        <div className=""> <Facebook content="Start my free trial"/> </div>
                    {/* </div> */}
                </Col>

                <Col lg={4} className={"m-auto p-5"}>
                    <Image src={LANDING_IMG} style={{ maxWidth: '700px'}}/>
                </Col>
                
                <Col lg={2} />
            </Row>


            {/* CARDS */}
            <Row className="d-flex justify-content-around mb-5 mr-5 ml-5 mt-5">

                <Col className="d-flex">
                    <div className="m-auto landing-card" style={{backgroundColor: "white"}}>
                        <Row className="d-flex mb-3">
                            <Image src={MIND_ICON} fluid className="mr-auto landing-card-icon"/>
                        </Row>
                        <Row>
                            <p className="landing-card-title">Own machine learning</p>
                        </Row>

                        <Row>
                            <p className="landing-card-desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo </p>
                        </Row>

                        <Row>
                            <Link to="#" className="landing-card-btn">Learn more</Link>
                        </Row>
                    </div>
                </Col>

                <Col className="d-flex">
                    <div className="m-auto landing-card" style={{backgroundColor: "white"}}>
                        <Row className="d-flex mb-3">
                            <Image src={CHATBOT_ICON} fluid className="mr-auto landing-card-icon"/>
                        </Row>
                        <Row>
                            <p className="landing-card-title">Chatbot solution</p>
                        </Row>

                        <Row>
                            <p className="landing-card-desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo </p>
                        </Row>

                        <Row>
                            <Link to="#" className="landing-card-btn">Learn more</Link>
                        </Row>
                    </div>
                </Col>

                <Col className="d-flex">
                    <div className="m-auto landing-card" style={{backgroundColor: "white"}}>
                        <Row className="d-flex mb-3">
                            <Image src={CORE_AI_ICON} fluid className="mr-auto landing-card-icon"/>
                        </Row>
                        <Row>
                            <p className="landing-card-title">Exceptional Core AI</p>
                        </Row>

                        <Row>
                            <p className="landing-card-desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo </p>
                        </Row>

                        <Row>
                            <Link to="#" className="landing-card-btn">Learn more</Link>
                        </Row>
                    </div>
                </Col>
                
            </Row>
        </Container>
        </>
    )
}

export default LandingPage;
