import React, {useEffect, useState} from 'react';
import './Login.css';
import { FacebookProvider, LoginButton } from 'react-facebook';
import axios from 'axios';
import { host } from '../../config';
import Swal from 'sweetalert2/dist/sweetalert2.js';


import {
  Redirect,
  useParams,
} from "react-router-dom";

function Facebook(props) {

  const [redirect, setRedirect] = useState(false);
  let { tokenMembership } = useParams();
  
  // Popup config
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'wizard-pages-active-btn-alert',
    },
    buttonsStyling: false
  });



  const handleResponse = (data) => {
    
    let token = data.tokenDetail.accessToken;
    let tokenMemberShip = tokenMembership ? tokenMembership : null;

    axios.post(host + '/api/v1/auth/login/facebook', {
        token,
        tokenMemberShip
    }).then( async resdata => {
      // console.log('Login =>', resdata)
      await localStorage.setItem('authtoken', resdata.data.data.user.idFacebook)
      await localStorage.setItem('userData',JSON.stringify(resdata.data.data));
      setRedirect(true);

    }).catch((err) => {
      console.log("Login Error =>", err)
      swalWithBootstrapButtons.fire({
        icon: 'error',
        title: 'Connexion error with the server try again !',
        confirmButtonText: 'Okay',
      });
    })

  };


  const handleError = (error) => {
    console.log(error);
  }

  return (
    <>
      { redirect && <Redirect to="/home/pages" /> }

      <FacebookProvider appId="783983335073350" version="v3.0">
        <LoginButton
          scope="public_profile,publish_pages,email,pages_messaging,pages_manage_cta,pages_show_list,manage_pages,instagram_basic,instagram_manage_comments,ads_management,ads_read"
          onCompleted={handleResponse}
          onError={handleError}
          className="facebookbtn"
        >
          <span>{props.content.toUpperCase()}</span>
        </LoginButton>
      </FacebookProvider>
    </>
  );
}

export default Facebook;
